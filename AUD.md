# Effizienz von Algorithmen
Algorithmen sollen sein:
* korrekt
* effizient
```
algorithm untätig(n):
    for i <- 1, ..., n:
        warte eine Minute

algorithm sum(a[1…n] Array von Zahlen):
    s <- 0
    for i <- 1, ..., n:
        s <- s+a[i]
    return s
```
Diese beiden Algorithmen haben eine lineare Laufzeit.
```
algorithm minDP(a[1…n] Array von Zahlen)
    m <- a[1]
    if n > 1
    for i <- 2, ..., n
        if a[i] < m
            m <- a[i]
    return m
```
best-case: a[1...n] = [1, 2, 3, 4, ..., n−1, n]
worst-case: a[1...n] = [n, n−1, n−2, ..., 2, 1]

Laufzeitanalysen
T(n) = Laufzeit bei Eingabelänge n
Sei $n \in \mathbb{N}$ gegeben:
* best-case: niedrigstes T(n)
* worst-case: höchstes T(n)
average-case

**Rechenaufgaben**
100000 · 100000 = 10000000000

```
algorithm minBF(a[1…n]):
    for i <- 1, ..., n:
        if isLowerBound(a[i], a[1…n]):
            return a[i]

algorithm isLowerBound(x, a[1…n]):
    for j <- 1, ..., n:
        if a[j] < x:
            return false
    return true
```
best-case:a$[1...n]=\underbrace{[1, 2, ..., 2]}_{n-1 mal}$
=> lineare Laufzeit
worst-case: a$[1...n] = \underbraces{[1, 2, ..., 2]}_{n-1 mal}$
=> quadratische Laufzeit
```
algorithm quadraschUntäg(n):
    for i <- 1, ..., n:
        for j <- 1, ..., n:
            warte eine Minute
```
## Vergleich worst-case Laufzeit
minDP vs. sum?
minDP vs. minBF?

## asymptotische Laufzeit
## Laufzeiten vergleichen
n = Eingabegröße
T(n) = worst-case Laufzeit

```
algorithm sum(a[1…n]):
    s <- 0
    for i <- 1, ..., n:
        s <- s + a[i]
    return s

algorithm product(a[1…n]):
    p <- 1
    for i <- 1, ..., n:
        p <- p*a[i]
    return p

algorithm product2(a[1…n]):
    p <- 1
    for i <- 1, ...,n:
        p <- p*a[i]*a[i]
    return sqrt(p)
```
## Asymptotische Laufzeit
wächst überlinear

## Lineare Laufzeit
wächst linear
T(n) wächst höchstens linear
* es gibt ein c mit $T(n) \leq c · n$ für alle $n \in \mathbb{N}_0$
* es gibt ein c mit $T(n) \leq c · n$ für alle genügend große $n \in \mathbb{N}_0$
* es gibt ein c mit $T(n) \leq c · n$ für alle genügend großen $n \in \mathbb{N}_0$

## allgemein
Sei f(n) eine Funktion
* "T(n) wächst höchstens so stark wie f(n)"
* es gibt ein c mit $T(n) \leq c · f(n)$ für alle genügend große $n \in \mathbb{N}$
* T(n) ist O(f(n))
* O ist ein Landau Symbol
### Beispiele
* $f(n) = n => O(f(n)) = O(n) =>$ "linear"
* $f(n) = n^2 => O(f(n)) = O(n2) =>$ "quadrasch"
* $T(n) = 3n ist O(n) und erst recht O(n^2)$
* $T(n) = 5n^2 ist O(n^2) aber nicht O(n)$

O(f(n)) enthält alle Funkionen, die höchstens so schnell wie f(n) wachsen.
### Eigenschaften von O
1. es ist eine Abschätzung
2. ... nach Oben
3. es sind Außnahmen erlaubt
4. ... aber nur endlich viele
5. der Faktor c ist "versteckt"
6. ... kann aber auch groß sein
7. c ist konstant, d.h. unabhängig von n

### Laufzeitklassen
* O(1): "konstant"
* O(log n): "logarithmisch"
* $O(\sqrt{n}) = O(n\frac{1}{2})$
* $O(n^2)O(n)$
* $O(n) \subseteq O(n^2)$
* $O(n)$: "linear"
* $O(n log n)$
* $O(n^2)$: "quadrasch"
* $O(n^3)$: "kubisch"
* $O$(n^k)$: "polynomiell" ($k \geq 0$ konstant)
* $O(k^n)$: "exponenel

# Faustregeln für O
## Regel 1: Um die Laufzeit eines Algorithmus zu besmmen, muss man ihn verstehen

```
algorithm alg1(n):
    return n
    z <- 0
    for i <- 1, ..., n:
        z <- z + i
    return z
```

## Regel 2: Schleifen können Laufzeit verursachen
```
algorithm alg2a(n): #0(n)
    z <- 0
    for i <- 1, ..., n:
        z <- z + i
    return z

algorithm alg2b(n): #O(n)
    z <- 0
    for i <- 1, ..., n^2:
        z <- z + i
return z

algorithm alg2c(n): #O(n)
    z <- 0
    i <- 1
    while i \leq n^2:
        z <- z + i
        i <- i + 1
return z
```
### Regel 2': ... Rekursion auch
```
algorithm alg2d(n)
    if n = 1:
        return 1
    else:
        return alg2d(n−1) + n
```
### Regel 3: Schachtelung => Mulplikation
```
algorithm alg3a(n): O(n^2)
    z <- 0
    for i <- 1, …, n: O(n^2)
        for j <- 1, …, n: O(n)
            z <- z + i · j # O(1)
return z

algorithm alg3b(n): #O(n^5)
    x <- 1
    for k <- 1, ..., n^3 #O(n^5)
        x <- x · alg3a(n) #O(n^2)
return x
```
### Achtung: manchmal ist das Ergebnis nur eine grobe obere Schranke

```
algorithm min(a[1…n]):
    i <- 1
    while i \leq n #O(n^2)
        ismin <- true
        for j <- i+1, ..., n: #O(n)
            if a[j] < a[i]: #O(1)
                ismin <- false
                i <- j
                break
        if ismin
            return a[i]
```
Laufzeit von min ist $O(n^2)$  
Aber wenn man genau hinschaut, findet man heraus: dieser Algorithmus hat auch eine Laufzeit von 
O(n)
### Regel 4: Hintereinander ⇒ Addion
```
algorithm alg4(n):
    z <- 0
    for i <- 1, ..., n: #O(n)
        z <- z + i #O(1)
    for j <- 1, …, n^2: #O(n)
        z <- z + 2^j #O(1)
return z
```
### Regel 5: bei einer Summe dominiert der Summand mit dem stärksten Wachstum
$O(3 · n^2 + n^3 + 1000000 · n^{2.6} + n log n + 1000)
= O(n^3)$
### Regel 6: Konstante Faktoren kann man weglassen
$O(13 n^2) = O(n^2)$
#### Beispiel: alles die gleiche Laufzeitklasse:
* $O(n^2)$
* $O(3n^2)$
* $O(8n^2 + 5n + 3)$
* $O(2n + 8n^2 + 2n^2)

# Tutorien Woche 2
## Fehlerhaer Algorithmus 1
Der folgende Algorithmus sollte alle geraden Zahlen aus a[1…n] summieren.
```
algorithm sumEven(a[1…n]):
    for i <- 1, ..., n:
        sum <- 0
        if a[i] ist gerade:
            sum <- sum + a[i]
    return sum
```
* Frage: Was gibt der Algorithmus stattdessen zurück?
* Antwort: a[n], wenn das gerade ist, ansonsten 0
## Fehlerhafter Algorithmus 2
Der folgende Algorithmus `isIncreasing` sollte prüfen, ob eine Liste a[1…n] aufsteigend 
sortiert ist:
```
algorithm isIncreasing(a[1...n]):
    if n < 2:
        return true
    for i <- 1, ..., n−1:
        if a[i] > a[i+1]:
            return false
        else
            return true
```
* Frage: Was gibt der Algorithmus stattdessen zurück?
* Antwort: `true`, wenn n < 2 oder $a[1] \leq a[2]$, ansonsten `false`
"plötzlicher Schleifentod"!
## Fehlerhafter Algorithmus 3
Der folgende Algorithmus max sollte das Maximum der Zahlen in einem Arrays a[1…n] berechnen:
```
algorithm max(a[1…n]):
    m <- 1
    for i <- 2, ..., n:
        if i > m:
        m <- i
return m
```
* Frage: Was gibt der Algorithmus sta dessen zurück?
* Antwort: n
### Die zweitkleinste Zahl
Beschreiben Sie einen Algorithmus min2 in Pseudocode, der den zweitkleinsten Wert zurückgibt, 
der in einer Liste a[1…n] von $n \geq 1$ Zahlen vorkommt. So soll zum Beispiel 
min2([1, 11, 4, 1, 6, 4]) den Wert 4 zurückgeben. Wenn alle Zahlen in a gleich sind, es also 
keinen zweitkleinsten Wert gibt, soll Ihr Algorithmus $+\infty$ zurückgeben.
### Lösungsansatz 1:
Besmme zunächst das Minimum m. Laufe dann nochmals durch das Array und suche nach der kleinsten 
Zahl $\neq$ m in a; das ist dann die zweitkleinste Zahl.
```
algorithm min2(a[1…n]):
    m <- +\infty
    for i <- 1, ..., n:
        if a[i] < m
        m <- a[i]
        m <- min(a[1...n])
    m2 <- +\infty
    for i <- 1, ..., n:
        if a[i] \neq m and a[i] < m2:
            m2 <- a[i]
    return m2
```
### Lösungsansatz 2:
Bestimme zunächst das Minimum m. Ändere alle Vorkommen von m in a in $+\infty$ um. Suche danach 
nochmal das Minimum in a.
```
algorithm min2(a[1…n]):
    m <- min(a[1...n])
    for i <- 1, ..., n:
        if a[i] = m:
            a[i] <- +\infty
    return min(a[1…n])
```
### Lösungsansatz 3: 
Zunächst wird das Array a[1…n] aufsteigend sorert und dann wird nach der zweitkleinsten Zahl 
gesucht.
```
algorithm min2(a[1…n]):
    sort(a[1…n]) <- aufsteigend soreren
    if n > 1:
    for i <- 2, ..., n:
        if a[i] > a[1]:
            return a[i]
return +\infty
```
### Lösungsansatz 4: 
Laufe einmal über das Array und merke dir immer die kleinste und die zweitkleinste bisher
gesehene Zahl.
```
algorithm min2(a[1…n]):
    m <- a[1] <- "Minimum"
    m2 <- +\infty <- "zweitkleinste Zahl"
    for i <- 2, ..., n:
        if a[i] < m:
            m2 <- m
            m <- a[i]
        else if a[i] < m2 and a[i] \neq m:
            m2 <- a[i]
return m2
```
## Faustregeln zur asymptotischen Laufzeit:
1. Der Algorithmus algo1 bestehe aus drei Teilen, die nacheinander ausgeführt werden:
```
algorithm algo1(a[1…n])
Teil1(a[1…n])
Teil2(a[1…n])
Teil3(a[1…n])
```
Teil1 habe eine Laufzeit von O(n · log n), Teil2 eine Laufzeit von O(n2) und Teil3 eine 
Laufzeit von O(n). Welche Laufzeit hat algo1 insgesamt? Geben Sie eine möglichst enge obere 
Schranke für die asymptotische worst-case Laufzeit von algo1 (in O-Notaon) in Abhängigkeit 
von der Eingabelänge n an.  
**Antwort**: $O(n^2)$  

2. Ein zweiter Algorithmus algo2 rufe algo1 genau 3n + 2 mal hintereinander auf:
```
algorithm algo2(a[1…n]):
    for i <- 1, ..., 3n + 2
algo1(a[1…n]) <= O(n^2)
```
Welche Laufzeit hat algo2? Geben Sie eine möglichst enge obere Schranke für die asymptotische 
worst-case Laufzeit von algo2 (in O-Notaon) in Abhängigkeit von der Eingabelänge n an.
Antwort: $O((3n + 2) · n^2) = O(3n^3 + 2n^2) = O(3n^3) = O(n^3)$
### Laufzeiten 1
Der folgende Algorithmus algo erhält als Eingabe eine natürliche Zahl $n \in \mathbb{N}$ und ruft 
wiederholt den Algorithmus sub auf:
```
algorithm algo(n):
    for i <- 1, ..., n^2:
        sub(n)
    for j <- 1, ..., n:
        sub(n^3)
```
1. Welche Laufzeit hat algo, wenn sub eine konstante Laufzeit O(1) besitzt, die Laufzeit von 
sub also unabhängig von der Eingabe ist? Geben Sie eine möglichst enge obere Schranke für die 
worst-case Laufzeit von algo (in O-Notaon) in Abhängigkeit von n an.
```
algorithm algo(n):
    for i <- 1, ..., n^2:
        sub(n)
    for j <- 1, ..., n:
        sub(n3)
```
Die erste Schleife braucht $O(n^2)$, die zweite $O(n)$ => insgesamt $O(n^2)$

2. Welche Laufzeit hat algo, wenn sub eine lineare Laufzeit besitzt, wenn also für alle 
$m \in \mathbb{N}$ gilt: sub(m) hat eine Laufzeit von $O(m)$? Geben Sie eine möglichst enge 
obere Schranke für die worst-case Laufzeit von algo (in O-Notaon) in Abhängigkeit von n an.
```
algorithm algo(n):
    for i <- 1, ..., n^2:
        sub(n)
    for j <- 1, ..., n:
        sub(n3)
```
Die erste Schleife braucht $O(n^3)$, die zweite Schleife braucht $O(n^4)$, also insgesamt 
$O(n^4)$

## Laufzeiten 2
Betrachten Sie folgenden Algorithmus algo, der einen anderen Algorithmus calculate aufruft:
```
algorithm algo(a[1…n]):
    sum <- 0
    for i <- 1, ..., n:
        sum <- sum + calculate(i, a[1…n])
    return sum
```
Entscheiden Sie zu jeder der folgenden Aussagen, ob diese wahr ist oder falsch:
1. Wenn die Laufzeit von calculate $O(n^2)$ ist, dann ist die Laufzeit von algo $O(n^3)$.
2. Wenn die Laufzeit von calculate $O(n^2)$ ist, dann ist die Laufzeit von algo $O(n^5)$.
3. Wenn calculate schnell genug läuft, dann ist es möglich, dass algo in O(n · log n) läuft.
Antwort: Ja, wenn calculate eine Laufzeit von O(log n) hat.
4. Wenn calculate eine polynomielle Laufzeit hat, dann läuft auch algo in polynomieller Zeit.
**Antwort**: Ja, denn wenn calculate eine Laufzeit von $O(n^k)$ hat, dann hat algo eine Laufzeit 
von $O(n ·n^k) = O(n^{k+1})$ => auch die Laufzeit von algo ist polynomiell.
## Groß-O-Notation
Entscheiden Sie zu jeder der folgenden Aussagen, ob diese richtig oder falsch ist:
1. $5 · n · log(n) + n ist O(n^2)$
2. $5 · n · log(n) + n ist O(n · log n)$
3. $5 · n · log(n) + n ist 𝒪(n · \sqrt{n})$
4. $n +\frac{1}{8} · n^2 ist O(n · log n)$
5. $nk ist O(k^n)$ für alle konstanten $k > 1$
6. Für jede Zahl $n \in \mathbb{N}$ gibt es eine Zahl c mit $13 · n2 \leq c · n$
## Rätsel: Schach-Champion
Eine Gruppe von $k \geq 3$ Schachspielern will wissen, wer von Ihnen der beste Schachspieler 
ist. Darum spielt jeder der k Spieler gegen jeden anderen, und zwar so lange, bis einer den  
anderen besiegt hat. Das Ergebnis wird in einer Tabelle A festgehalten: Das Tabellenfeld 
A[i, j] ist = 1, wenn der Spieler i gegen Spieler j gewonnen hat. Hat hingegen Spieler i gegen 
Spieler j verloren, so ist A[i, j] = 0, dafür ist in diesem Fall aber natürlich A[j, i] = 1.
Außerdem definieren wir: A[i, i] = 1. Insgesamt stehen in A also $k^2$ Zahlen, d.h. die 
Eingabelänge ist n = $k^2$. Ein Champion ist ein Spieler, der gegen alle anderen Spieler 
gewonnen hat. Beschreiben Sie einen Algorithmus (in Pseudocode), der als Eingabe A erhält und 
daraus ermielt, ob es einen Champion gibt. Dabei soll Ihr Algorithmus eine Laufzeit von 
$O(k) = $O(\sqrt{n})$ haben.
