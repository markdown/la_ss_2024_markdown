## 5 Lineare Algebra
### 5.8 Vektorräume und Lineare Unabhängigkeit
Es ist leicht vorstellbar, dass man statt Vektorrechnung mit drei Koordinaten ebenso gut auch eine solche für 4, 5, . . . oder gleich allgemein für n Koordinaten entwickeln kann, nur geht die Anschauung dabei natürlich verloren. Auch kann man für die Koordinaten statt reeller Zahlen auch komplexe
verwenden.
**Def**: $ \mathbb{R} \left\{ 
\begin{pmatrix}
u_1 \\ u_2 \\ \dots \\ u_n
\end{pmatrix}: u_1, \dots, u_n \in \mathbb{R} \right\}, \mathbb{C}^n = 
\left\{\begin{pmatrix} 
z_1, z_2, \dots, z_n \end{pmatrix}: z_1,\dots,z_n \in \mathbb{C} \right\}$. Die Elemente dieser
Mengen heißen Vektoren oder genauer Spaltenvektoren mit n reellen bzw. komplexen Koordinaten.
Auf $\mathbb{R}^n$ bzw. $\mathbb{C}^n$ werden die Addition zweier Vektoren, Multiplikation mit einem Skalar und Substraktion genau wie im anschaulichen Fall des $\mathbb{R}^3$ koordinatenweise erklärt. Im Fall des $\mathbb{R}^n$ sind Skalare reelle Zahlen, für den $\mathbb{C}^n$ komplexe Zahlen. Ganz offensichtlich gelten die dort aufgeführten Rechenregeln auch in diesem allgemeineren Fall. Eine solche Menge zusammen mit diesen Operationen und Rechenregeln nennt man Vektorraum. Die Definition des Skalarprodukts über Koordinaten lässt sich ganz zwangslos verallgemeinern:
**Def.**: Für zwei Vektoren $\overrightarrow{u} = (u_1 u_2 \dots u_n) T, \overrightarrow{v} = (v_1 v_2 \dots v_n)^T \it \mathbb{R}^n$ ist ihr Skalarprodukt $\langle \overrightarrow{u}|\overrightarrow{v} \rangle = \overrightarrow{u}*\overrightarrow{v}=\sum^{n}_{k=1} v_k u_k.$ Für $\overrightarrow{u} \in \mathbb{R}^n$ ist sein Betrag (euklidische Norm) $|\overrightarrow{u}|=\sqrt{\langle \overrightarrow{u}|\overrightarrow{v} \rangle}$.  
Bei komplexen Koordinaten muss die Definition etwas verändert werden:  
**Def**.: Für zwei Vektoren $\overrightarrow{z} = (z_1 z_2 \dots z_n)^T,\overrightarrow{w} = (w_1 w_2 \dots w_n)^T \in \mathbb{C}^n ist ihr Skalarprodukt $langle \overrightarrow{z}|\overrightarrow{w} \rangle =\overrightarrow{z}*\overrightarrow{w} = \sum^{n}_k=1 z_k \overline{w}_k$. Für \overrightarrow{z} \in \mathbb{C}^n ist sein Betrag (euklidische Norm) |\overrightarrow{z}| = \sqrt{\langle \overrightarrow{z}|\overrightarrow{z} \rangle}$.  

Die komplexe Konjugation ist hier nötig, damit $\langle \overrightarrow{z}|\overrightarrow{z} \rangle$ stets reell und $\geq 0$ wird, denn sonst gäbe es Probleme mit der Quadratwurzel beim Betrag.
Mit der Kunjugation wird nicht einheitlich verfahren, man findet ebenso häufig die Variante, wo
die Konjugation nicht bei den Koordinaten des zweiten Vektors (wie oben), sondern denen des
ersten Vektors steht. Im Prinzip spielt das keine Rolle, solange man das konsequent betreibt, und
bei den Rechenregel (i) unten rechts $\lambda$ durch $\overline{\lambda}$ ersetzt und umgekehrt be 
(iv). Im $\mathbb{R}^n$ ist das Skalarprodukt kommutativ, d. h. $\langle \overrightarrow{u}| 
\overrightarrow{v} \rangle = \langle \overrightarrow{v}|\overrightarrow{u} \rangle$, im 
$\mathbb{C}^n$ jedoch nicht, dort ist $\langle \overrightarrow{z}| \overrightarrow{w} \rangle = 
\langle \overrightarrow{w}|\overrightarrow{z} \rangle$. Auch deshalb ist die Schreibweise $\langle 
\overrightarrow{z}|\overrightarrow{w} \rangle$ für das Skalarprodukt üblich. Da für reelle Zahlen 
die komplexe Konjugation keine Wirkung hat, kann man sich Fallunterscheidungen sparen, wenn man 
sich durchgängig die „komplexe Variante“ des Skalarprodukts
angewöhnt.
Es ergibt also Sinn, beim Skalarprodukt peinlichst auf die Reihenfolge der Faktoren zu achten!
Die folgenden Sätze sind stets auf den komplexen Fall ausgelegt, der reelle ist damit jeweils
automatisch mit abgedeckt.  
  
Der folgende Satz folgt sofort aus den bekannten Rechenregeln für komplexe Zahlen:
Satz: Beim oben definierten Skalarprodukt gelten für beliebige $\overrightarrow{z},
\overrightarrow{w} \in \mathbb{C}^n$ und $\lambda \in \mathbb{C}.  
i. $\langle \lambda\overrightarrow{z}| \overrightarrow{w} \rangle = \lambda \langle 
\overrightarrow{z}|\overrightarrow{w] \rangle$  
ii. $\langle \overrightarrow{z}_1 + \overrightarrow{z}_2|\overrightarrow{w} \rangle = \rangle \overrightarrow{z}_1 | \overrightarrow{w} \rangle + \langle \overrightarrow{z}_2 | \overrightarrow{w} \rangle$  
iii. $\langle \overrightarrow{z} | \overrightarrow{w} \rangle = \overline{\langle 
\overrightarrow{w} | \overrightarrow{z}} \rangle$  
iv. $\langle \overrightarrow{z} | \lambda \overrightarrow{w} \rangle = \overline{\lambda} \langle 
\overrightarrow{z} | \overrightarrow{w} \rangle$  
v. $\langle \overrightarrow{z} | \overrightarrow{w}_1 + \overrightarrow{w}_2 \rangle = \langle 
\overrightarrow{z} | \overrightarrow{w}_1 \rangle + \langle \overrightarrow{z} | 
\overrightarrow{w}_2 \rangle$  
vi. $\langle \overrightarrow{z} | overrightarrow{z} \rangle$ ist reel und $\geq 0$  
Über den Betrag (euklidische Norm) ergibt sich in einfacher Weise auch eine Längen- oder Ab-
standsmessung, die folgende aus der Anschauung vertraute Eigenschaften aufweist:
Satz: Für beliebige $\overrightarrow{z},\overrightarrow{w} \it \mathbb{C}^n$ und $\lambda \in 
\mathbb{C}$ gilt  
(i) $| \overrightarrow{z} | \geq 0,$ und $|\overrightarrow{z} | = 0$ nur für 
$\overrightarrow{z} = 0$  
(ii) $|\lambda \overrightarrow{z} | = |\lambda| · |\overrightarrow{z}|$  
(iii) $|\langle \overrightarrow{z} | \overrightarrow{w} \rangle| \leq 
|\overrightarrow{z} |*|\overrightarrow{w} |$  
(iv) $|\overrightarrow{z} + \overrightarrow{w} | \leq |\overrightarrow{z} | + 
|\overrightarrow{w} |$  
Beweis von (iii):31 Der Fall $|\overrightarrow{w} | = 0$ ist trivial, daher sein nun 
$|\overrightarrow{w}|\neq 0$. Für $\lambda \in \mathbb{C}$ ist  
$z + \lambda\overrightarrow{w} |^2 = \langle \overrightarrow{z} + \lambda\overrightarrow{w} | 
\overrightarrow{z} + \lambda \overrightarrow{w} \rangle$   
$= \langle \overrightarrow{z} | \overrightarrow{z} \rangle + \overline{\lambda} \langle 
\overrightarrow{z} |\overrightarrow{w} \rangle + \lambda \langle \overrightarrow{w} |
\overrightarrow{z} \rangle + \langle \overline{\lambda}\langle \overrightarrow{w} | 
\overrightarrow{w} \rangle$  
$= |\overrightarrow{z} |^2 + \overline{\lambda} \langle \overrightarrow{z} | \overrightarrow{w} 
\rangle + 
\underbrace{\lambda \overline{\langle \overrightarrow{z}|\overrightarrow{w}}}_{\overline{
\overline{\lambda}\langle \overrightarrow{z}| \overrightarrow{w}}} \lambda \overline{\lambda}| 
\overrightarrow{w}|^2 =|\overrightarrow{z}|^2 +2 \Re (\overline{\lambda} \langle|\overrightarrow{z}
| \overrightarrow{w} \rangle)+|\lambda|^2 |\overrightarrow{w}|^2$  
und Einsetzen von $\lambda = (-1/|\overrightarrow{w}|^2)* \langle \overrightarrow{z}| 
\overrightarrow{w} \rangle$ liefert dann  
$0 \leq | \overrightarrow{z} + \lambda \overrightarrow{w} |^2 = | \overrightarrow{z}|^2 -2 
\frac{| \langle \overrightarrow{z} | \overrightarrow{w} \rangle|^2}{| \overrightarrow{w}|^2} +
\frac{|\langle \overrightarrow{z}| \overrightarrow{w} \rangle |^2 }{| \overrightarrow{w}|^4}*| 
\overrightarrow{w}|^2 = \frac{| \langle \overrightarrow{z} | \overrightarrow{w} \rangle|^2}{| 
\overrightarrow{w} |^2}$.  
Mit $| \overrightarrow{w} |^2$ multipliziert erhält man $0 \leq | \overrightarrow{z} |^2 · |
\overrightarrow{w} |^2 − |\langle \overrightarrow{z} | \overrightarrow{w} \rangle|^2$, woraus die 
Behauptung folgt.  
**Beweis von (iv)**:32 Mit der CAUCHY-SCHWARZschen Ungleichung (iii) ergibt sich zunächst  
$|\overrightarrow{z} +\overrightarrow{w} |^2 = \langle \overrightarrow{z} + \overrightarrow{w} |
\overrightarrow{z} + \overrightarrow{w} \rangle = \langle \overrightarrow{z} | \overrightarrow{z} 
\rangle + \overrightarrow{z} | \overrightarrow{w} \rangle + \overrightarrow{w} | 
\overrightarrow{z} \rangle + \overrightarrow{w} | \overrightarrow{w} \rangle$  
$= | \overrightarrow{z} |^2 + 2 \Re \langle \overrightarrow{z} | \overrightarrow{w} \langle + 
| \overrightarrow{w} |^2 \leq | \overrightarrow{z} |^2 + 2 | \langle \overrightarrow{z} | 
\overrightarrow{w} \rangle| + | \overrightarrow{w} |^2$  
$\leq | \overrightarrow{z} |^2 + 2 | \overrightarrow{z} | · |\overrightarrow{w} | + |
\overrightarrow{w} |^2 = |\overrightarrow{z} | + | \overrightarrow{w} |^2$ ,  
und durch Wurzelziehen (alle beteiligten Terme sind reell nichtnegativ) folgt die Behauptung.  
Aufgrund der Eigenschaft (iii) kann man über das Skalarprodukt eine Winkelmessung einführen:  
**Def.**: Der von $\overrightarrow{z},\overrightarrow{w} \in \mathbb{R}^n \ { \overrightarrow{0} }
eingeschlossene Winkel $\varphi$ ist definiert durch cos $\varphi = $\frac{\langle 
\overrightarrow{z} | \overrightarrow{w} \rangle}{| \overrightarrow{z} | · | 
\overrightarrow{w} |}$. 
Zwei Vektoren $\overrightarrow{z}, \overrightarrow{w} \in \mathbb{C}^n heißen orthogonal, wenn 
$\langle \overrightarrow{z} | \overrightarrow{w} \rangle = 0$ ist.  

Der Begriff "orthogonal" ergibt auch im komplexen Fall durchaus Sinn, während "eingeschlos-
sener Winkel" nur im reellen Fall brauchbar ist. Allerdings hat bei mehr als drei reellen Koodi-
naten dieser Winkel keine anschauliche Bedeutung mehr $\dots$  
**Def.**: Sind $\overrightarrow{z}_1,\overrightarrow{z}_2, \dots ,\overrightarrow{z}_m \in 
\mathbb{C}^n$ irgendwelche Vektoren und $\lambda_1, \lambda_2, \dots \lambda_m \in \mathbb{C}$ 
irgendwelche Zahlen, so nennt man  
$\sum^{m}_{k=1} \lambda_k \overrightarrow{z}_k = \lambda_1 \overrightarrow{z}_1, lambda_2
\overrightarrow{z}_2, + \dots + \lambda \overrightarrow{z}_m$  
eine Linearkombination der Vektoren $\overrightarrow{z}_1,\overrightarrow{z}_2, \dots ,
\overrightarrow{z}_m$. Die Menge aller möglichen Linearkombination
von $\overrightarrow{z}_1, \dots ,\overrightarrow{z}$ m heißt lineare Hülle dieser Vektoren, abgekürzt span $( \overrightarrow{z}_1, \dots ,\overrightarrow{z}_m)$.  
**Def**.: Die Vektoren $\overrightarrow{z}_1,\overrightarrow{z}_2, \dots ,\overrightarrow{z}_m 
\in \mathbb{C}^n$ heißen linear unabhängig, wenn die Gleichung  
$\lambda_1 \overrightarrow{z}_1 + \lambda_2 \overrightarrow{z}_2 + \dots + \lambda_m 
\overrightarrow{z}_m = \overrightarrow{0}$  
für die Unbekannten $\lambda_1, \dots , \lambda_m$ nur die triviale Lösung $\lambda_1 = \lambda_2
= \dots = \lambda_m = 0$ hat. Anderenfalls nennt man sie linear abhängig.  
Offensichtlich gilt: Sind $\overrightarrow{z}_1,\overrightarrow{z}_2, \dots ,\overrightarrow{z}$ m linear unabhängig, so gilt 
dies erst recht für jede Teilmenge dieser Vektoren.  
Satz: Die Vektoren $\overrightarrow{z}_1, \overrightarrow{z}_2, \dots ,\overrightarrow{z}_m \in \mathbb{C}^n$ sind genau dann 
linear abhängig, wenn mindestens einer dieser Vektoren eine Linearkombination der restlichen ist.  
**Beweis**: Angenommen,$\overrightarrow{z}_1,\overrightarrow{z}_2, \dots ,\overrightarrow{z}_m$ sind linear abhängig. Dann gibt 
es eine Linearkombination  
$\sum^{m}_{k=1} \lambda_k \overrightarrow{z}_k = \overrightarrow{0}$, wo mindestens eine der Zahlen $\lambda_1, \dots , 
\lambda_m$, etwa $\lambda_r$, ungleich Null ist. Durch Um stellen folgt nun $\overrightarrow{z}_r = \frac{1}{\lambda_r}
\sum^{m}_{k=1  k \neq r} \lambdaλ_k \overrightarrow{z}_k$, mithin ist $\overrightarrow{z}_r$ eine Linearkombination der restlichen Vektoren.  
Nun sei umgekehrt etwa $\overrightarrow{z}_r$ eine Linearkombination der restlichen Vektoren, also  
$\overrightarrow{z}_z=\sum^{m}_{k=1  k \neq r} \lambda_k \overrightarrow{z}_k$ mit irgendwelchen Zahlen $\lambda_1,\dots,
\lambda_{r-1},\lambda_{r+1},\dots,\lambda_m$. Mit $\lambda_r = -1$ dann  
$\overrightarrow{0} = \underbrace{\lambda_r}_{-1} \overrightarrow{z_r}+\sum^{m}_{k=1 k \neq r} \lambda_k \overrightarrow{z}_k =  
\sum^m_{k=1} \lambda_k \overrightarrow{z}_k$ also sind $\overrightarrow{z}_1, \overrightarrow{z}_2,\dots,\overrightarrow{z}_m$ 
linear abhängig.  

Über die Eigenschaft im letzten Satz, dass sich irgendeiner der Vektoren als Linearkombination der anderen ausdrücken läßt bzw. 
deren Negation könnte man natürlich auch lineraren (Un-) Abhängigkeit definieren. Dies hätte aber den gravierenden Nachteil, 
dass man überprüfen müsste, ob der erste Vektor eine Linearkombination der anderen ist, ob das für den zweiten zutrifft, für
der dritten usw. Das ist offensichtlich nicht sinnvoll! Die Definition oben ist dagegen gerade so gemacht, dass einerseits der 
(Rechen-) Aufwand zur Prüfung möglichst gering ist und andererseits auch alle Vektoren völlig gleichberechtigt vorkommen. Zudem 
muss man auch keine Klimmzüge machen, wenn man nur einen einzigen Vektor betrachtet, dieser Fall passt "nahtlos" mit hinein.
Es ergibt also Sinn, sich mit der auf den ersten Blick sonderbar anmutenden Definition anzufreunden. (Wer das nicht macht, wird 
das früher oder später mit viel Verdruß bezahlen.)  

**Def.**: Die Vektoren $\overrightarrow{z}_1, \dots ,\overrightarrow{z}_m \in \mathbb{C}^n$ bilden eine Basis des $\mathbb{C}^n$,
wenn sie linear unabhängig sind und für jeden weiteren Vektor $\overrightarrow{w} \in \mathbb{C}^n$ die Vektoren 
$\overrightarrow{z}_1, \dots , \overrightarrow{z}_m,\overrightarrow{w}$ linear abhängig sind.  
Alternative Formulierung: Eine Basis ist eine maximale linear unabhängige Menge von Vektoren.
Eine offensichtliche Basis des $\mathbb{C}^n$ ist die sog. Standardbasis $\overrightarrow{e}_1, \dots ,\overrightarrow{e}_n$, 
wo $\overrightarrow{e}_k$ in der k-ten Koordinate eine 1 und sonst nur Nullen hat. Man könnte sich nun fragen, ob es vielleicht 
noch kleinere oder größere (hinsichtlich Anzahl der Vektoren) Basen gibt. Aus den folgenden Sätzen ergibt sich jedoch,
dass dies nicht so ist:  
Satz:$\overrightarrow{z}_1,\dots,\overrightarrow{z}_m$ sei eine Basis,\overrightarrow{w} ein weiterer Vektor (desselben Vektorraumes). Dann ist $\overrightarrow{w}$ eine eindeutig bestimmte Linearkombination von $\overrightarrow{z}_1, \dots ,\overrightarrow{z}_m$  
Beweis: Da $\overrightarrow{z}_1, \dots ,\overrightarrow{z} m,\overrightarrow{w}$ linear abhängig ist, gibt es eine nicht triviale Linearkombination  
$\lambda_1 \overrightarrow{z}_1 + \dots + \lambda_m \overrightarrow{z}_m + \mu w = \overrightarrow{0}$ .  
Wäre $\mu = 0$, könnte man den Summanden mit $\overrightarrow{w}$ einfach weglassen, und man hätte eine nicht triviale
Linearkombination von $\overrightarrow{z}_1, \dots ,\overrightarrow{z}$ m allein, die $\overrightarrow{0}$ ergäbe, im 
Widerspruch zur linearen Unabhängigkeit von $\overrightarrow{z}_1, \dots ,\overrightarrow{z}_m$. Folglich ist $\mu \neq 0$ und 
damit  
$\overrightarrow{w}=frac{-\lambda_1}{\mu}\overrightarrow{z}_1 + \dots + \mu_m \overrightarrow{z}_m$  
womit die Existenz nachgewiesen ist. Nun werde angenommen, dass es zwei Linearkombinationen  
$\lambda_1 overrightarrow{z}_1 + \dots + \lambda_m \overrightarrow{z}_m = \overrightarrow{w} = \mu_1 \overrightarrow{z}_1 + 
\dots + \mu \overrightarrow{z}_m$  
wegen der linearen Unabhängigkeit von $ \overrightarrow{z}_1,\dots, \overrightarrow{z}_m$ dass $\lambda_1 = \mu_1, \dots, 
\lambda_m = \mu_m$ gilt.  
**Satz:** [Austauschsatz] $\overrightarrow{z}_1,\overrightarrow{z}_2,\dots,\overrightarrow{z}_m$ sei eine Basis, 
$\overrightarrow{w} \neq \overrightarrow{0}$ ein weiterer Vektor (desselben Vektorraumes), für den es nach dem vorigen Satz 
Zahlen $\lambda_1,\dots,\lambda_m$ gibt mit  
$\overrightarrow{w}=\lambda_1 \overrightarrow{z}_1+\lambda_2+\overrightarrow{z}_2+\dots+\lambda_m \overrightarrow{z}_m$.  
Dann gilt: Ist dabei $\lambda_k \geq 0$, so ist $\overrightarrow{z}_1, \dots, \overrightarrow{z}_{k-1},\overrightarrow{w},
\overrightarrow{z}_{k+1},\dots,\overrightarrow{z}_m$ wieder eine Basis.  
Da $\overrightarrow{w} \geq 0$ vorausgesetzt wird, ist natürlich mindestens eine der Zahlen $\lambda_1, \dots , \lambda_m$ von 
0 verschieden. Der Satz anders ausgedrückt: Man kann einen Vektor (aber keineswegs einen beliebigen!) der
Basis gegen $\overrightarrow{w}$ austauschen, ohne die Basiseigenschaft zu zerstören.  
**Beweis**: Um Schreibarbeit zu sparen, setzen wir (durch Umnummerieren) voraus, dass $\lambda_1 = 0$ ist. Jetzt ist 
nachzuweisen, dass $\overrightarrow{z}_1$ (nach Umnummerieren!) gegen$\overrightarrow{w}$ ausgetauscht werden kann. Zuerst zur 
linearen Unabhängigkeit. Gäbe es eine nicht triviale Linearkombination  
$\overrightarrow{0} = \mu_1 \overrightarrow{w} + \mu_2 \overrightarrow{z}_2 + \dots + \mu_m \overrightarrow{z}_m$ ,  
müsste $\mu_1 \geq 0$ sein, sonst könnte man ja $\mu_1 \overrightarrow{w}$ in obiger Summe durch $0\overrightarrow{z}_1$ 
ersetzen, im Widerspruch zur linearen Unabhängigkeit von $\overrightarrow{z}_1, \dots ,\overrightarrow{z}_m$. Also ergäbe sich
$\overrightarrow{w}=0 \overrightarrow{z}_1+\frac{-\mu_2}{\mu_1}\overrightarrow{z}_2 + \dots + \frac{-\mu_m}{\mu_1} 
\overrightarrow{z}_m$.  
was wegen der Eindeutigkeit der Darstellung von⃗w durch die Basis (voriger Satz) auf den Wider-
spruch $\lambda_1 = 0$ führt.  
$\overrightarrow{c}=\overrightarrow{\mu}_1 \overrightarrow{z}_1 + \dots + \mu_m \overrightarrow{z}_m$,  
also wegen $\overrightarrow{z}_1 = 1/ \lambda_1(\overrightarrow{w}-(\lambda_2 \overrightarrow{z}_2+\dots+ \lambda_m 
\overrightarrow{z}_m))$  
$\overrightarrow{0}=\overrightarrow{c} - \overrightarrow{c}= \mu_1 \overrightarrow{z}_1 + \mu_2 + \overrightarrow{z}_2 + \dots +
\mu_m + \overrightarrow{z}_m +(-1)\overrightarrow{c} = \frac{\mu_1}{\lambda_1} \overrightarrow{w} +(\mu_2 - \mu_1 
\frac{\lambda_2}{\lambda_1}) \overrightarrow{z}_1 + \dots + (\mu_m - \mu_1 frac{\lambda_m}{\lambda_1})\overrightarrow{z}_m + 
(-1)\overrightarrow{c}$.  
Dies wäre eine nicht triviale Linearkombination im Widerspruch zur Annahme.  
**Satz**: [Austauschsatz von STEINITZ] $\overrightarrow{z}_1,\overrightarrow{z}_1,\dots,\overrightarrow{z}_m$ sei eine Basis, 
$\overrightarrow{w}_1,\dots,\overrightarrow{w}_r$ irgendwelche linear unabhängigen Vektoren. Dann ist $r \leq m$ und nach geeigneter Umnummerierung der $\overrightarrow{z}_k$ ist $\overrightarrow{w}_1,\dots,\overrightarrow{w}_r,\overrightarrow{z}_{r+1}, 
\dots, \overrightarrow{z}_m$ wieder eine Basis.  
**Beweis**: Mittels vollständiger Induktion nach r:
**Induktionsanfang**: Für r = 1 ist die Aussage genau der vorhergehende Austauschsatz.
Induktionsschritt: Die Aussage gelte nun für jede r-elementige Menge linear unabhängiger Vektoren. Sind $\overrightarrow{w}_1, 
\dots, \overrightarrow{w}_{r+1}$ linear unabhängig, so erst recht auch $\overrightarrow{w}_1,\dots,\overrightarrow{w}_r$. 
Nach Induktionsannahme ist daher $r \leq m$ und (nach Umnummerierung) $\overrightarrow{w}_1,\dots,\overrightarrow{w}_r, 
\overrightarrow{z}_{r+1}, \dots, \overrightarrow{z}_m$ eine Basis. Wäre nun r = m, müsste $\overrightarrow{w}_1,\dots,
\overrightarrow{w}_r$ eine Basis sein, was aber der linearen Unabhängigkeit von $\overrightarrow{w}_1,\dots,
\overrightarrow{w}_{r+1}$ widerspräche. Folglich ist r < m und damit auch noch $r+ \leq m$, d. h. der erste Teil der Aussage 
gilt auch für r + 1. Da $\overrightarrow{w}_1,\dots,\overrightarrow{w}_r,\overrightarrow{z}_{r+1},\dots,\overrightarrow{z}_m$
eine Basis ist, muss $\overrightarrow{w}_{r+1}$ eine Linearkombination dieser Vektoren sein, etwa  
$\overrightarrow{w}_{r+1}=\mu_1 \overrightarrow{w}_1+\dots+\mu_r \overrightarrow{w}_r+ \lambda_{r+1} +\overrightarrow{z}_{r+1} +
\dots + \lambda_m \overrightarrow{z}_m$  
Wären dabei alle Zahlen $\lambda_{r+1}, \dots, \lambda_m$ gleich 0, wäre $\overrightarrow{w}_{r+1}$ bereits eine 
Linearkombination von $\overrightarrow{w}_1,\dots,\overrightarrow{w}_r$, im Widerspruch zur linearen Unabhängigkeit von 
$\overrightarrow{w}_1,\dots,\overrightarrow{w}_r,\overrightarrow{w}_{r+1}$. Durch Umnummerieren von 
$\overrightarrow{z}_{r+1},\dots,\overrightarrow{z}_m$ darf man also annehmen, dass $\lambda_{r+1} \neq 0$ ist. Obiger Austauschsatz besagt nun, dass $\overrightarrow{w}_1,\dots,\overrightarrow{w}_{r+1},\overrightarrow{z}_{r+2},\dots,\overrightarrow{z}_m$ 
wieder eine Basis ist, d. h. der zweite Teil der Aussage ist auch für r + 1 richtig.  
Folglich gilt die Aussage für alle $r \in \mathbb{N}$  
**Satz**: Je zwei Basen eines Vektorraums haben gleich viele Elemente.
**Beweis**: $\overrightarrow{z}_1,\overrightarrow{z}_2,\dots,overrightarrow{z}_m$ bzw. $\overrightarrow{w}_1,\dots,
\overrightarrow{w}_r$ seien Basen desselben Vektorraums. Nach dem Austausch-
satz von STEINIZ folgt $r \leq m$. Vertauscht man die Rollen der beiden Basen, ergibt sich ebenfalls $m \leq r$, also muss r = m sein.  
**Def.**: Die (nach vorstehendem Satz eindeutig bestimmte) Anzahl von Elementen einer Basis eines
Vektorraumes heißt Dimension dieses Vektorraumes.  
Da die Standardbasis des $\mathbb{C}^n$ genau n Elemente hat, gilt dies für jede Basis des $\mathbb{C}^n$, die Dimension des 
$\mathbb{C}^n$ ist gleich n.  
**Def.**: Ein System von Vektoren $\overrightarrow{z}_1,\overrightarrow{z}_2,\dots,\overrightarrow{z}_m \in \mathbb{C}^n$ heißt 
Orthogonalsystem, wenn für alle
k, l = 1, . . . , m mit $k \neq l$ stets $\langle \overrightarrow{z}_k | \overrightarrow{z}_l \rangle =0$ und keiner der 
Vektoren der Nullvektor ist.  
Dies kann man auch so ausdrücken: Je zwei verschiedene dieser Vektoren sind stets orthogonal, und
der Nullvektor ist nicht mit dabei.  
**Satz**: Ist $\overrightarrow{z}_1, \overrightarrow{z}_2,\dots,\overrightarrow{z}_m \in \mathbb{C}^n$ ein Orthogonalsystem, so 
sind $\overrightarrow{z}_1,\overrightarrow{z}_2,\dots,\overrightarrow{z}_m$ linear unabhängig.  
**Beweis**: Angenommen $\sum^{m}_{k=1} \lambda_k \overrightarrow{z}_k = \overrightarrow{0}$ für 
irgendwelche Zahlen $\lambda_1,\dots,\lambda_m$ Dann folgt:  
$0=\langle \overrightarrow{0} | \overrightarrow{z}_r \rangle =\langle \sum^{m}_{k=1} \lambda_k 
\overrightarrow{z}_k | \overrightarrow{z}_r \rangle = \sum^{m}_{k=1} \lambda_k \langle \overrightarrow{z}_k | 
\overrightarrow{z}_r \rangle = \lambda_r |\overrightarrow{z}_r|^2$,  
da für $k \neq r$ stets $\langle \overrightarrow{z}_r | \overrightarrow{z}_k \rangle =0$ ist. Da $\overrightarrow{z}_r \neq 
\overrightarrow{0}$ ist, folgt $|\overrightarrow{z}_r| \neq 0$, also muss $\lambda_r = 0$ gelten. Da dieser Schluß für jedes 
$r=1,2,\dots,m$ möglich ist, ergibt sich die lineare Unabhängigkeit.  
**Def.** Ist $\overrightarrow{z}_1,\overrightarrow{z}_2,\dots,\overrightarrow{z}_m \in \mathbb{C}^n$ ein Orthogonalsystem, 
wobei zusätzlich alle diese Vektoren den Betrag 1 haben, so nennt man dieses System ein Orthonormalsystem (ONS).  
Ebenso wie die Vektorrechnung werden in der gesamten Matrizen- und Determinantenrechnung lediglich die Grundrechenarten für 
reelle Zahlen verwendet, das gleiche gilt auch für LGSe. Daher ist es ziemlich offensichtlich, dass man genauso gut auch 
überall komplexe Zahlen statt nur reeller Zahlen verwenden kann, was nun auch vorausgesetzt sei.  
**Satz**: Ist $A=(a_{k,r})$ eine (n, n)-Matrix, so ist det(A) genau dann = 0, wenn die Spalten von A (als Vektoren aus dem 
$\mathbb{C}^n$ betrachtet) linear abhängig sind.  
Da det(A) = det($A^T$) ist, gilt dieser Satz natürlich analog auch für die Zeilen der Matrix A.  
Beweis: Zunächst werde angenommen, die Spalten (-vektoren) seien linear abhängig. Dann lässt sich mindestens eine Spalte als 
Linearkombination der anderen schreiben. Zieht man nun die entsprechenden Vielfachen jener Spalten von dieser einen ab, so 
ändert sich einerseits der Wert der Determinante nach der bekannten Rechenregel ja nicht, andererseits erhält man hinterher 
eine Nullspalte. Folglich ist die Determinante = 0.  
Nun sei umgekehrt die Determinante = 0. Dann kann das LGS $A\overrightarrow{z} = \overrightarrow{c}$ niemals eindeutig lösbar
sein, sondern — je nach rechter Seite $\overrightarrow{c}$ - entweder gar nicht oder es gibt unendlich viele verschiedene 
Lösungen. Da das LGS $A\overrightarrow{z}= \overrightarrow{0}$ aber schon die Lösung $\overrightarrow{z} = 
\overrightarrow{0}$ hat, muss der zweite Fall eintreten. Es gibt also einen Vektor $\overrightarrow{z} = 
(\lambda_1,\lambda_2,\dots,\lambda_m)^T$, wo nicht alle $\lambda_k$ gleich 0 sind, so dass $A\overrightarrow{z} = 
\overrightarrow{0}$ ist. Bezeichnet man nun die Spalten von A mit $\overrightarrow{a}_1,\overrightarrow{a}_2,\dots,
\overrightarrow{a}_n$, so ist $\overrightarrow{0}=A\overrightarrow{z} =\sum^{n}_{k=1} \lambda_k \overrightarrow{a}_k$, also gibt es eine nicht triviale Linearkombination der Spalten, die⃗ 0 ergibt, d. h. die Spalten sind
linear abhängig.
