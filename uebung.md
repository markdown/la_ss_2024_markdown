# Hörsaalübungen

 

## 1.

 

Ein Körper hat keine so genannte Nullteiler (Definition und Nachweis finden Sie im Skript, Seite 110 f). Weisen Sie für die Menge der komplexe Zahlen \C nach, dass

 

### (a)

es mit der Multiplikation wie aus der VL keine Nullteiler gibt, während

### (b)

es bei der „naiven“ Definition einer Multiplikation gemäß

(x_1+y_1 j)

\cdot(x_2+y_2 j)

=(x_1 \cdot x_2)

+(y_1 \cdot y_2) j

 

jedoch Nullteiler gäbe.

 

Die scheinbar komplizierte Definition der Multiplikation komplexer Zahlen wie in der VL ist also nötig...

 

Beim ersten Teil kann man entweder direkt vorgehen (mühsam und unübersichtlich) oder man lässt sich vom allgemeinen Beweis der Nullteilerfreiheit bei Körpern (siehe Skript) inspirieren. Natürlich dürfen Sie diesen Satz selbst nicht verwenden, denn Sie sollen das gerade für diesen Spezialfall „,zu Fuß“ nachweisen. Was in der Definition eines Körpers ausdrücklich steht (sprich: Gruppe bezüglich Addition beziehungsweise — ohne die Null — bezüglich Multiplikation und das Distributivgesetz) dürfen Sie jedoch verwenden.

 

Beim zweiten Teil sollte man ein konkretes Beispiel angeben.

 

## 2.

 

Zur Wiederholung ... Weisen Sie nach:

### (a)

\Z_7={0,1,2, ..., 6} versehen mit der üblichen Addition und Multiplikation, allerdings modulo 7, ist ein Körper. Geben Sie dabei auch an, wie die neutralen und inversen Elemente aussehen.

### (b)

\Z_8={0,1,2, ..., 7} versehen mit der üblichen Addition und Multiplikation, allerdings modulo 8, ist kein Körper.

 

Da die Mengen hier doch noch recht übersichtlich sind, ist es wohl am einfachsten, die inversen Elemente (so vorhanden) konkret anzugeben. Etwas unangenehm wird allerdings die Assoziativität, denn da sind ja drei Elemente beteiligt, das heißt es gibt bereits 7^3 beziehungsweise 8^3 verschiedene Möglichkeiten. Deshalb sollte man sich hier überlegen, was diese Modulo-Rechnung eigentlich bewirkt, und nicht etwa alle Möglichkeiten abklappern ...

 

## 3.

 

Ein naheliegender Grund für die „Erfindung“ komplexer Zahlen ist, dass man quadratische Gleichungen gern ohne Einschränkung lösen können möchte:

 

Berechnen Sie die Lösungen der folgenden quadratischen Gleichungen, z. B. mit der wohlbekannten p-q-Formel. Wenn etwas Negatives unter der Wurzel herauskommt, muss man an j^2=-1 denken und etwas improvisieren. Damit Sie sich sicher sind, dass das wirklich erfolgreich war, machen Sie bitte in jedem Fall die Einsetzprobe!

 

(a) x^2+16=0

 

(b) x^2+6 x+13=0

 

(c) 4 x^2+16 x+80=0

 

[Originalseite 2]

 

## 4.

 

Weisen Sie nach: Ist V ein Vektorraum über einem Körper \K (Achtung, nicht notwendig \R oder \C, sondern ein beliebiger Körper!), so gelten

### (a)

0 \odot \vec{v}=\vec{0} für alle \vec{v} \in V

### (b)

\lambda \odot \vec{0}=\vec{0} für alle \lambda \in \K

Das mag ziemlich selbstverständlich aussehen, ist aber dennoch beweiswürdig. Sie dürfen dabei über den Körper \K und den Vektorraum V außer den in den Definitionen genannten Eigenschaften und den bekannten Sätzen (Skript Seite 100-102) nichts voraussetzen.

 Wenn man etwa an den \R^3 über \R denkt, sind beide Aussagen natürlich offensichtlich, Sie sollen diese aber eben nicht für ein konkretes Beispiel nachprüfen, sondern sich vergewissern, dass diese tatsächlich allgemeingültig sind! Und für irgendeinen abstrakten Vektorraum ist das halt nicht so ganz offensichtlich.

 

Insbesondere: 0 ist nicht notwendig die bekannte Zahl 0 , sondern „nur“ das neutrale Element bezüglich der Addition in \K, entsprechend \vec{0} "nur" das neutrale Element bezüglich der Addition in V.

 

Der Deutlichkeit halber benutzen Sie bitte für die Addition in V und die Multiplikation mit einem Skalar durchgängig ""\oplus, \odot"" im Gegensatz zu "+" für die Operationen in \K.

 

Hilfreich für diese Beweise sind insbesondere die beiden Distributivgesetze in der Definition eines Vektorraumes ...

 

Bei den folgenden Aufgaben (und generell für die Zukunft) können wir auf die Unterscheidung von "\oplus, \odot" für die Operationen auf V im Gegensatz zu "+,\cdot" für die Operationen in \K verzichten, denn dies ist aus dem Zusammenhang klar. Auch wird "\odot" meist ganz weggelassen.

 

5.

 

Weisen Sie mittels vollständiger Induktion nach:

 

Ist U ein UVR eines Vektorraumes (V, \K,+, \cdot), so gilt für jedes n \in \N={1,2,3, ...} :

 

Sind \lambda_1, \lambda_2, ..., \lambda_n \in \K, \vec{u}_1, \vec{u}_2, ..., \vec{u}_n \in U, so ist auch \sum_{k=1}^n \lambda_k \vec{u}_k \in U.

 

Es wird hier ein auch formal korrekter Induktionsbeweis erwartet! Beachten Sie bitte, dass in der Definition eines UVRes diese Aussage nur für "n=2" verlangt wird. Die Fälle "n=1" und "n \geq 2" müssen also getrennt bearbeitet werden.

 

6

 

Weisen Sie nach: Ist U ein UVR des Vektorraumes (V, \K,+, \cdot), so ist auch (U, \K,+, \cdot) ein Vektorraum.

 

Hinweis: Das sieht ja vielleicht völlig offensichtlich aus, aber hier sollen tatsächlich alle geforderten Eigenschaften eines Vektorraumes einzeln überprüft werden! Denken Sie bitte insbesondere daran, dass die inverse Elemente bezüglich "+" von den Elementen aus U ja in U liegen müss(t)en.

 

[Originalseite 3]

 

## 7.

 

Entscheiden Sie jeweils, ob U ein UVR von V ist (Nachweis!):

 

(a) V=\R^4,

U={(\begin{array}{l}x_1 \\ x_2 \\ x_3 \\ x_4\end{array}) \in \R^4 |\, x_1+2 x_2+3 x_3=0}

 

(b) V=\R^3, U={(\begin{array}{l}x_1 \\ x_2 \\ x_3\end{array}) \in \R^3 |\, x_1+2 x_2=1}

 

(c) V=\R^2, U={(\begin{array}{l}x_1 \\ x_2\end{array}) \in \R^2 |\, x_1 \cdot x_2=0}

 

(d) V=\R^4, U={(\begin{array}{c}x_1 \\ x_2 \\ x_3 \\ x_4\end{array}) \in \R^4|| x_1|=| x_2 |}

 

Das "|x_1|" und so weiter steht für den sog. Betrag einer reellen Zahl:

 

|x|= \begin{cases}+x, & falls x \geq 0 \\

-x, & falls x<0\end{cases}

 

Zum Beispiel ist |-3|=+3 und |+4|=+4.

 

(e) V=\R^3, U={(\begin{array}x_1 \\ x_2 \\ x_3\end{array}) \in \R^3 |\, x_1=x_2}

 

(f) V=C([0,1]), U={f \in V \mid f(0)=0=f(1)}

 

Mit C([0,1]) ist die Menge (und Vektorraum!) der stetigen Funktionen auf dem Intervall [0,1] gemeint. Stetig heißt anschaulich: Man kann den Funktionsgraphen der Funktion (über das Intervall von 0 bis 1) in "einem Zug" ohne Absetzen zeichnen.

 

Für diese Aufgabe ist das nicht weiter von Bedeutung, man könnte auch den Vektorraum der (beliebigen) auf dem Intervall [0,1] definierten Funktionen betrachten. Nur möchte man meistens mit den Vektoren (hier Funktionen) noch irgend etwas „,anfangen“, und sei es nur, den Funktionsgraphen zu zeichnen, und Funktionen ohne jede Einschränkung können recht „wild“ aussehen ...

 

## 8.

 

Überprüfen Sie die Menge aus den folgenden Vektoren auf lin. Unabhängigkeit und geben Sie eine maximale (hinsichtlich Anzahl der Elemente) linear unabhängige Teilmenge an!

 

(\begin{array}1 \\3 \\5 \\7\end{array}),

 

(\begin{array}9 \\10 \\11 \\6\end{array}),

 

(\begin{array}1 \\1 \\1 \\1\end{array}),

 

(\begin{array}2 \\1 \\0 \\-3\end{array})

 

(Von der Formulierung her kann man schon vermuten, dass alle vier Vektoren zusammen linear abhängig sein werden, man aber vielleicht zwei oder gar drei davon auswählen kann, die dann linear unabhängig sind.)

 

[Originalseite 4]

 

## 9.

 

Überprüfen Sie die Menge aus den folgenden Vektoren auf lineare Unabhängigkeit!

 

(\begin{array}1 \\1 \\1 \\1 \\1 \\1 \\1\end{array}),

 

(\begin{array}1 \\0 \\2 \\0 \\3 \\0 \\4\end{array}),

 

(\begin{array}0 \\1 \\0 \\2 \\0 \\3 \\0\end{array}),

 

(\begin{array}1 \\1 \\0 \\0 \\2 \\2 \\0\end{array}),

 

(\begin{array}1 \\2 \\3 \\4 \\5 \\6 \\7\end{array}).

 

## 10

 

Überprüfen Sie die Menge der folgenden Vektoren auf lineare Unabhängigkeit! Der zugrunde liegende Vektorraum ist C([0,1]).

 

p_0(x)=1,

p_1(x)=x,

p_2(x)=x^2,

p_3(x)=x^3,

p_4(x)=x^4

 

Hinweis: Man sollte sich zuerst klar machen, was der Nullvektor hier ist. Wie bei einem Beispiel aus der VL sucht man sich danach am besten ein paar (wie viele?) „schöne“ Werte für die Variable x und setzt diese ein. Diese Werte müssen natürlich im Intervall von 0 bis 1 (jeweils inklusive) liegen, denn so ist der zugrunde liegende Vektorraum definiert, alles außerhalb ist für die Variable x "verboten".

 

Es geht zwar auch einfacher (ohne jede Rechnung), aber dazu muss man ein wenig über Linearfaktorzerlegung von Polynomen, Polynomdivision und so weiter wissen.

 [Originalseite 5]

## 11.

 

Zuerst eine kleine Definition: Sind U{1}, U{2} zwei UVRe von (V, \K, \oplus, \odot), ist ihre Summe

 

U_1+U_2=

{\vec{u}_1+\vec{u}_2

\mid \vec{u}_1 \in U_1,

\vec{u}_2 \in U_2}.

 

Weisen Sie nach, dass diese Summe von zwei UVRen U_1, U_2 wieder ein UVR ist. (Das geht natürlich genauso für eine analog definierte Summe von mehr als zwei UVRe, das brauchen Sie aber nicht nachzuweisen, da das durch eine offensichtliche vollständige Induktion mit der Aussage für zwei UVRe folgt.)

 

## 12

 

Auch hier zuerst eine kleine Definition: Sind U_1, U_2, ..., U_n jeweils UVRe von (V, \K, \oplus, \odot), so dass es zu jedem \vec{v} \in U_1+U_2+\cdots+U{n} stets eindeutig bestimmte Vektoren \vec{u}1 \in U_1, \vec{u}2 \in U_2, ..., \vec{u}n \in U{n} gibt mit \vec{v}=\vec{u}1+\vec{u}2+\cdots \vec{u}n, so nennt man die Summe U{1}+U{2}+\cdots+U{n} auch eine direkte Summe der UVRe, kurz U{1} \oplus U{2} \oplus \cdots \oplus U{n}.

 

### (a)

 

Weisen Sie nach: Eine Summe aus zwei UVRen U{1}, U{2} ist genau dann eine direkte Summe, wenn ihr Durchschnitt nur aus dem Nullvektor besteht, das heißt U{1} \cap U{2}={\vec{0}} ist.

 

### (b)

 

Überprüfen Sie am folgenden Beispiel mit V=\R^2, dass das offensichtliche Analogon (wie lautet das wohl genau?) bei einer Summe von drei UVRen nicht mehr richtig ist:

 

U_1={(\begin{array}x \0\end{array})|, x \in \R},

U{2}={.(\begin{array}0 \y\end{array})|, y \in \R},

U_3={.(\begin{array}x \x \end{array})|, x \in \R}

 [Originalseite 6]

## 13

 

### (a)

Gegeben seien die folgenden drei UVRe (das muss nicht nachgeprüft werden) des \R^3 :

 

U{1}={\lambda(\begin{array}1 \2 \3\end{array})|, \lambda \in \R},

U{2}={\lambda(\begin{array}1 \1 \1\end{array})|, \lambda \in \R},

U_3={\lambda(\begin{array}3 \2 \1\end{array})|, \lambda \in \R}

 

Was ist U{1}+U{2}+U{3} (kurz ausgedrückt)? Ist die Summe U{1}+U{2}+U{3} eine direkte Summe?

 

### (b)

Analoge Frage, aber diesmal mit

 

U_3={\lambda(\begin{array}3 \2 \3\end{array}) |, \lambda \in \R}

 

## 14.

 

Der Hintergrund für diese Aufgabe ist die sog. Bestapproximation: Im \R^3 suche man zu einem Punkt den nächst gelegenen Punkt einer Ebene. Idee: Man fälle das Lot von dem Punkt auf die Ebene. Exakt dasselbe Prinzip klappt in jedem Vektorraum, sofern „Abstand“ beziehungsweise „Länge“ über ein Skalarprodukt definiert wird.

 

Rechnen Sie nach: Ist V ein Vektorraum (mit dem Körper \R oder \C ) mit Skalarprodukt, \vec{v} \in V, ferner {\vec{e}{1}, \vec{e}{2}, ..., \vec{e}{n}} ein so genanntes Orthonormalsystem (kurz ONS) (das heißt Vektoren aus V mit \langle\vec{e}{k} \mid \vec{e{l}}\rangle=0 für k, l=1,2, ..., n, falls k \neq l ist, sowie \langle\vec{e}{k} \mid \vec{e{k}}\rangle=1 ), so gelten für beliebige Zahlen \mu{1}, ..., \mu_n :

 

### (a)

 

|\vec{v}-\sum{k=1}^n \mu{k} \vec{e}{k}|^2

=|\vec{v}|^2-\sum{k=1}^n \overline{\mu{k}}\langle\vec{v}

\mid \vec{e}{k}\rangle-\sum{k=1}^n \mu{k} \overline{\langle\vec{v}

\mid \vec{e}{k}\rangle}+\sum{k=1}^n \mu{k} \overline{\mu{k}}

 

Hier geht es den Betrag von Vektoren (über das Skalarprodukt definiert), dazu vgl. Eigenschaften eines Skalarproduktes beziehungsweise des darüber definierten Betrages im Skript S. 146.

 

### (b)

 

\sum{k=1}^n|\langle\vec{v}

\mid \vec{e}{k}\rangle-\mu{k}|^2

=\sum{k=1}^n|\langle\vec{v}

\mid \vec{e}{k}\rangle| ({2})

-\sum{k=1}^n \overline{\mu{k}}\langle\vec{v}

\mid \vec{e}{k}\rangle-\sum{k=1}^n \mu{k}

\overline{\langle\vec{v} \mid \vec{e}{k}\rangle}

+\sum{k=1}^n \mu{k} \overline{\mu_k}

 

Diesmal geht es jedoch um den Betrag von komplexen Zahlen, dazu vgl. Skript S. 17-20.

 

Diese beiden Gleichungen haben erst einmal *keinerlei* Zusammenhang untereinander, gewisse Ähnlichkeiten der rechten Seiten sind rein zufällig.

[Originalseite 7] 

 ## 15

 

Weisen Sie nach, dass die Menge aller Folgen (a{k}){k=1}^{+\infty} mit \sum|a_k|<+\infty mit den in offensichtlicher Weise definierten Rechenoperationen inklusive Skalarprodukt ein Vektorraum mit Skalarprodukt ist.

 

Hinweis: Am einfachsten sucht man sich einen etwas „größeren“ Vektorraum ... Die Menge dieser Folgen beziehungsweise diesen Vektorraum nennt man l_1(\R) beziehungsweise l_1(\C).

## 16

 Bekanntlich ist \R^n beziehungsweise \C^n ein Vektorraum mit Skalarprodukt (dem üblichen). Folgern Sie aus den in der VL bewiesenen Eigenschaften eines (beliebigen!) Skalarproduktes:

 Für beliebige (reelle oder auch komplexe) Zahlen a_1, ... a_n, b_1, ... b_n gilt:

\sum{k=1}^n |a_k \cdot b_k|

\leq \sqrt{\sum{k=1}^n |a_k|^2}

\cdot \sqrt{\sum{k=1}^n |b_k|^2}

(Dies ist die beim Nachweis der Vektorraumeigenschaft von l_2(\C) benötigte Ungleichung, sie wird häufig ebenfalls als CAUCHY-SCHWARZsche Ungleichung bezeichnet.)

## 17

 Wenden Sie das GRAM-SCHMIDTsche Orthonormalisierungsverfahren auf die folgenden Vektoren (in der angegebenen Reihenfolge!) und überprüfen Sie anschließend, ob Sie wirklich ein ONS erhalten haben!

 (\begin{array}1 \1 \0 \0\end{array}),

 (\begin{array}1 \0 \1 \0\end{array}),

 (\begin{array}1 \1 \1 \0\end{array}),

 (\begin{array}1 \1 \1 \1\end{array})

 ## 18

 Wenden Sie das GRAM-SCHMIDTsche Orthonormalisierungsverfahren auf die Vektoren auf Aufgabe 9 (in der angegebenen Reihenfolge!) an und überprüfen Sie anschließend, ob Sie wirklich ein ONS erhalten haben!

 Hinweis: Es ist ratsam, den zweiten Teil gleich „nebenbei“ während des ersten Teils mit durchzuführen, um Rechenfehler frühzeitig zu erkennen. Die jeweils erste Koordinate der w{k} (das heißt vor dem Normieren) ist 1,-3 / 7,-12 / 11,83 / 230,-41 / 171, die Betragsquadrate der w{k} sind 7,110 / 7,46 / 11,513 / 115,14 / 57. Die Rechnung ist im Prinzip sehr einfach, wenn man das Prinzip verstanden hat, nur sind die sich ergebenden Brüche etwas hässlich ...

 ## 19

 Wenden Sie das GRAM-SCHMIDTsche Orthonormalisierungsverfahren auf die folgenden Vektoren (in der angegebenen Reihenfolge!) an! Der zugrunde liegende Vektorraum ist C([-1,1]) mit dem Skalarprodukt:

 \langle f \mid g\rangle

=\int_{-1}^{+1} f(x) \cdot \overline{g(x)} d x

Da die gesamte Rechnung hier (zufällig) sowieso reell ist, spielt die komplexe Konjugation hier keine Rolle.

p{0}(x)=1,

p{1}(x)=x,

p{2}(x)=x^2,

p{3}(x)=x^3,

p{4}(x)=x^4,

p_5(x)=x^5

[Originalseite 8] 

Hinweis: Diese Rechnung ist zwar nicht schwierig, aber manuell doch recht mühsam. Viele der benötigten Integrale sind offensichtlich gleich Null (ungerade Funktion über über ein symmetrisches Intervall integriert).

Es gibt eine nette Rekursionsformel, mit dem man die Ergebnisse ganz bequem der Reihe nach ausrechnen kann, s. Skript unter „LEGENDRE-Polynome“. Zur Kontrolle: Wenn Sie richtig gerechnet haben, sollte Ihnen eine gewisse Ähnlichkeit Ihrer Ergebnisse zu diesen LEGENDRE-Polynome (vgl. „Tabelle LEGENDRE-Polynome“ am Ende des Skripts) auffallen. Andere Möglichkeit: Man überprüfe am Ende, dass die w_k (die noch nicht normierten!) tatsächlich paarweise orthogonal sind.

Diese LEGENDRE-Polynome sind z. B. ganz nützlich, wenn man „irgendwelche“ Funktionen näherungsweise durch Polynome darstellen will.

[Originalseite 9] 

## 20

Gegeben sei ein Vektorraum V und UVRe U_1, ..., U_n von V, die paarweise orthogonal sind, das heißt für beliebige k, l \in{1, ..., n} mit k \neq l und beliebige Vektoren \vec{u}_k \in U{k}, \vec{u}_l \in U_l ist \langle\vec{u}_k \mid \vec{u}_l\rangle=0. Ferner sei U=U_1+U_2+...+U_n.

[Fußnote: Dazu sagt man: U ist die orthogonale direkte Summe der UVRe U_1, ... U_n, kurz U=U_1 \circled{\perp} U_2 \circled_{\perp} ... \circled_{\perp} U_n.]

Weisen Sie nach, dass dann gilt: Jeder Vektor \vec{u} \in U besitzt eine *eindeutige* Zerlegung \vec{u}=\vec{u}_1+\vec{u}_2+\cdots+\vec{u}_n, wo \vec{u}_1 \in U_1, \vec{u}_2 \in U_2, ..., \vec{u}_n \in U_n ist.

## 21 

Im Vektorraum V=\C^3 mit dem „Standard"-Skalarprodukt

 \langle (\begin{array}z_1 \\ z_2 \\ z_3 \end{array})|,

(\begin{array}}w_1 \\ w_2 \ w_3\end{array})\rangle

=\sum{k=1}^3 z_k \overline{w_k}

und

\vec{v}_{1}=(\begin{array}1 \ j \ -1\end{array}),_

\vec{v}_2=(\begin{array}1 \\ 1 \\ 1\end{array})

betrachte man den UVR U{1}=\operatorname{span}(\vec{v}{1}, \vec{v}_2).

Bestimmen Sie den (ja, der ist eindeutig bestimmt!) UVR U{2}, so dass V=U{1}(1) U_2 (orthogonale direkte Summe, vergleiche. Aufgabe 20) ist.

Hinweis: GRAM-SCHMIDT, beachten Sie bitte beim Skalarprodukt besonders die komplexe Konjugation bei den Koordinaten des zweiten Vektors.

Sofern Sie andere Literatur, ein CA-System zur Kontrolle o. Ä. verwenden: Die Handhabung der Konjugation ist nicht einheitlich, bisweilen steht die bei den Koordinaten des ersten Vektors (da ist auch die Definition des Skalarproduktes entsprechend anders herum). So lange man das einheitlich macht, ist das natürlich egal ...

## 22 

Im Vektorraum V=\C^5 mit dem ,Standard"-Skalarprodukt

 \langle (\begin{array}z_1 \\ z_2 \\ z{3} \ z{4} \ z{5}\end{array})|,

(\begin{array}}w_1 \\ w_2 \\ w_3 \\ w_4 \\ w_5\end{array})\rangle
=\sum{k=1}^5 z_k \overline{w_k}

und

\vec{v}_1=(\begin{array}1 \\ 1 \\ j \ 0 \\ 0\end{array}),

\vec{v}_2=(\begin{array}1 \\ 0 \\ 0 \\ j \\ 1\end{array}),

\vec{v}_3=(\begin{array}}1 \\ j \\ 1 \\ 1 \\ 1\end{array})

bilde man den UVR U_1=span(\vec{v}_1, \vec{v}_2, \vec{v}_3). Bestimmen Sie die Bestapproximation des Vektors \vec{p}=(\begin{array}1 \\ 2 \\ 3 \\ 4 \\ 5\end{array}) in U_1. 

Hinweis: GRAM-SCHMIDT und

(\begin{array}_2 \\ -1 \\ -j \\ 3 j \\ 3\end{array}),

(\begin{array}2+2 j \\ -1+7 j \\ 9-j \\ 5-5 j \\ 3+3 j\end{array}),

(\begin{array}76-32 j \\ 9+39 j \\ 85-7 j \\ 63+5 j \\ 75-9 j\end{array}).

[Originalseite 10]

 

 \setcounter{enumi}{22}

 

Ist V ein Vektorraum mit Skalarprodukt, kann man über dieses Skalarprodukt bekanntlich eine „sinnvolle“ Längen- beziehungsweise Abstandsmessung mittels |\vec{v}|=\sqrt{\langle\vec{v} \mid \vec{v}\rangle} erhalten.

 

Nun kann man allerdings auch durchaus auf andere Weise eine Längenmessung bewerkstelligen, etwa im \R^3 mittels

 

|\vec{v}|=|(\begin{array}{c}

 

v{x} \v{y} \v_z

 

\end{array})|=|v{x}|+|v{y}|+|v_z|

 

(Zur Unterscheidung sind hier statt der einfachen Betragsstriche Doppelstriche als Symbol für die Länge eines Vektors verwendet.)

 

Diese Längenmessung erfüllt auch die Anforderungen an eine sinnvolle Längenmessung, das heißt

 

(a) |\vec{v}| \geq 0 für alle \vec{v} \in \R^3 und |\vec{v}|=0 genau bei \vec{v}=\vec{0}

 

(b) |\lambda \vec{v}|=|\lambda| \cdot|\vec{v}| \quad für alle \lambda \in \R und alle \vec{v} \in \R^3

 

(c) ||\vec{u}+\vec{v}|\leq| \vec{u}|+| \vec{v} | für alle alle \vec{u}, \vec{v} \in \R^3

 

Auf der anderen Seite gibt es im \R^3 ja nicht nur ein Skalarprodukt, sondern durchaus mehr Auswahl, z. B.

 

\langle.(\begin{array}{l}

 

u{x} \u{y} \u_z

 

\end{array}) |\,(\begin{array}{l}

 

v{x} \v{y} \v_z

 

\end{array})\rangle=3 u{x} v{x}+5 u{y} v{y}+7 u{z} v{z}

 

und allein dadurch schon verschiedene Längenmessungen.

 

Folglich wäre es interessant zu wissen, ob man die obige Längenmessung ||\vec{v} | vielleicht durch irgendein ,exotisches“ Skalarprodukt bekommen kann oder nicht. Dazu:

 

(a) Rechnen Sie nach, dass für jeden über |\vec{v}|=\sqrt{\langle\vec{v} \mid \vec{v}\rangle} (beliebiger Vektorraum V mit Skalarprodukt!) Betrag eines Vektors stets die sog. Parallelogrammgleichung gilt:

 

|\vec{u}+\vec{v}| ({2}+|\vec{u}-\vec{v}|){2}=2(|\vec{u}| ({2}+|\vec{v}|){2}) \quad \text { für alle } \vec{u}, \vec{v} \in V

 

(b) Überlegen Sie, woher wohl der Name Parallelogrammgleichung kommt. Zeichnung für den Fall V=\R^3 ist hilfreich.

 

(c) Geben Sie ein konkretes Beispiel von Vektoren \vec{u}, \vec{v} \in \R^3 an, wo die oben definierte Längenmessung |\vec{v}| diese Parallelogrammgleichung nicht erfüllt.

 

Konsequenz: Die Längenmessung |\vec{v}| kann nicht von einem Skalarprodukt stammen.

 

(d) Angenommen, man hat irgendeine Längenmessung (nicht notwendig von einem Skalarprodukt), die die obigen Anforderungen an eine solche erfüllt und die die Parallelogrammgleichung erfüllt. Wie kann man daraus ein Skalarprodukt bekommen (Dazu unterstellt man einfach, dass sie von einem Skalarprodukt stammt und rechnet gewissermaßen rïckwärts)?

 

Konsequenz: Eine Längenmessung erfüllt genau dann die Parallelogrammgleichung, wenn sie von einem Skalarprodukt stammt.

 

Hier betrachten Sie bitte nur den reellen Fall. Zwar geht das auch bei einem komplexen Vektorraum, ist aber dort deutlich komplizierter.

 

Der Nachweis, dass man damit tatsächlich ein Skalarprodukt mit den geforderten Eigenschaften erhält, ist nicht ganz einfach. Die Eigenschaften (i) und (iii) sind noch offensichtlich.

 

(ii) dagegen macht viel Mühe: Zunächst rechnet man

 

\langle\vec{u}+\vec{v} \mid \vec{w}\rangle=\langle\vec{u} \mid \vec{w}\rangle+\langle\vec{v} \mid \vec{w}\rangle

 

nach, dazu benötigt man die Parallelogrammgleichung mehrfach und muss geschickt (um-) klammern \vec{u}+\vec{v}+\vec{w}=(\vec{u}+\vec{v})+\vec{w}=(\vec{u}+\vec{w})+\vec{v}, die Rollen von \vec{u}, \vec{v} vertauschen usw.

 

Hat man erst mal \langle\vec{u}+\vec{v} \mid \vec{w}\rangle=\langle\vec{u} \mid \vec{w}\rangle+\langle\vec{v} \mid \vec{w}\rangle, so folgt daraus \langle n \vec{u} \mid \vec{w}\rangle=n\langle\vec{u} \mid \vec{w}\rangle für natürliche Zahlen n. Damit wiederum ergibt sich dann leicht, dass das auch für Null und negative ganze Zahlen n und sogar \langle(p / q) \vec{u} \mid \vec{w}\rangle=(p / q)\langle\vec{u} \mid \vec{w}\rangle für alle Brüche p / q gilt. Zum Schluß überlegt man, dass \langle\lambda \vec{u} \mid \vec{w}\rangle sich nur wenig ändern kann, wenn man an der Zahl \lambda,,wackelt“, daraus folgt dann \langle\lambda \vec{u} \mid \vec{w}\rangle=\lambda\langle\vec{u} \mid \vec{w}\rangle für beliebiges reelles \lambda.

 

Hinweis: Die Teile (a) bis (c) sind ziemlich einfach. (d) ist eine Knobelaufgabe, wenn Sie da einen Teil nicht hinbekommen, können Sie den betreffenden Teil ja einfach als nachgewiesen betrachten und den Rest angehen.

 

 \setcounter{enumi}{23}

 

Welche Struktur hat die Lösungsmenge? Bestimmen Sie danach die Lösungsmenge in Vektorform! Verwenden Sie genau die Vorgehensweise aus der Vorlesung!

 

(a)

 

\begin{aligned}

 

& (\begin{array}{rrrrr|r}

 

-2 & 9 & 0 & 4 & 2 & 0 \0 & 0 & 3 & 3 & 1 & 4 \0 & 0 & 0 & -2 & 0 & 2 \0 & 0 & 0 & 0 & 2 & 0 \0 & 0 & 0 & 0 & 0 & 4

 

\end{array}) \& \square \text { keine, } \square \text { genau eine, }

 

\end{aligned}

 

\square - unendlich viele, frei wählbar:

 

(b) (\begin{array}{rrrrr|r}2 & 1 & -2 & 2 & 9 & 2 \ 0 & 0 & 0 & -3 & -2 & 3\end{array})

 

\square-keine, \square genau eine, \square unendlich viele, frei wählbar:

 

(c) (\begin{array}{rrrrr|r}0 & 2 & 2 & 3 & -2 & 1 \ 0 & 0 & -1 & -2 & 5 & 8 \ 0 & 0 & 0 & 3 & 5 & 7 \ 0 & 0 & 0 & 0 & 3 & 0 \ 0 & 0 & 0 & 0 & 0 & 0\end{array})

 

\square-keine, \square genau eine, \square unendlich viele, frei wählbar:

 

(d) (\begin{array}{rrr|r}-3 & 2 & 6 & 4 \ 0 & 5 & -3 & 5 \ 0 & 0 & 2 & 1 \ 0 & 0 & 0 & -1\end{array})

 

\square-keine, \square - genau eine, \square - unendlich viele, frei wählbar:

 

(e) (\begin{array}{rrr|r}5 & 3 & 3 & 3 \ 0 & -2 & 3 & 0 \ 0 & 0 & -9 & 1 \ 0 & 0 & 0 & 0 \ 0 & 0 & 0 & 0\end{array})

 

\square-keine, \square genau eine, \square unendlich viele, frei wählbar:

 

(f) (\begin{array}{rrrr|r}4 & 2 & 1 & 1 & -5 \ 0 & 3 & -2 & -2 & 2 \ 0 & 0 & -3 & 5 & 0 \ 0 & 0 & 0 & 7 & 0\end{array})

 

\square-keine, \square genau eine, \square —unendlich viele, frei wählbar:

 

 \setcounter{enumi}{24}

 

Bestimmen Sie mit Hilfe des GAUSSschen Algorithmus die Lösungsmengen der folgenden linearen Gleichungssysteme!\

 

(a) (\begin{array}{rrr|r}1 & 2 & 3 & 4 \ -3 & -6 & -8 & -10 \ 2 & 4 & 2 & 2 \ 1 & 2 & 5 & 14\end{array})\

 

(b) (\begin{array}{rrrrr|r}3 & 1 & 2 & 4 & 1 & 9 \ 9 & 3 & 6 & 15 & 8 & 29 \ 21 & 7 & 14 & 19 & -8 & 57\end{array})\

 

(c) (\begin{array}{rrrr|r}-1 & 1 & 3 & 1 & 9 \ -3 & 4 & 7 & 0 & 29 \ 1 & 1 & -12 & -2 & -5 \ 2 & 1 & -17 & -3 & -6\end{array})

 

Gegeben seien die Matrizen

 

A=(\begin{array}{rrr}

 

3 & 1 & 9 \-5 & 7 & 1 \5 & 4 & -2

 

\end{array}) \quad B=(\begin{array}{rrr}

 

1 & 3 & 2 \-2 & -2 & 7 \-3 & 2 & -5 \-1 & 0 & 1

 

\end{array}) \quad C=(\begin{array}{rrrr}

 

2 & -3 & 2 & 1 \-3 & 4 & 0 & 6 \8 & 9 & 1 & -2

 

\end{array})

 

Berechnen Sie die folgenden Ausdrücke (sofern möglich)!\

 

(a) B C+A\

 

(b) C B-A\

 

(c) C B A\

 

(d) B C A

 

 \setcounter{enumi}{26}

 

Gegeben seien die Matrizen

 

\begin{aligned}

 

& A=(\begin{array}{rrr}

 

1 & 3 & 4 \-2 & 3 & 3 \1 & -2 & -3 \-2 & -3 & -4

 

\end{array}) \quad B=(\begin{array}{rrrr}

 

7 & -2 & 1 & 0 \4 & -1 & 5 & 2 \3 & 1 & -1 & 4

 

\end{array}) \quad C=(\begin{array}{rrr}

 

-2 & 1 & 0 \4 & -1 & 2 \3 & -1 & 4

 

\end{array}) \& D=(\begin{array}{rrrr}

 

1 & -2 & 3 & 5 \-3 & -1 & 0 & 3

 

\end{array}) \quad E=(\begin{array}{rr}

 

1 & -2 \5 & 1 \9 & -7 \0 & 3

 

\end{array}) \quad F=(\begin{array}{lll}

 

1 & 3 & 5

 

\end{array}) \quad G=(\begin{array}{r}

 

1 \-2 \-3 \-1

 

\end{array})

 

\end{aligned}

 

Berechnen Sie (falls möglich!) die folgenden Ausdrücke (erst überlegen, wie es am einfachsten geht!).\

 

(a) A \cdot B \cdot G\

 

(c) D \cdot E\

 

(e) B \cdot A+C\

 

(b) E \cdot D\

 

(d) F \cdot C\

 

(f) F \cdot C \cdot F

 

 \setcounter{enumi}{27}

 

Weisen Sie mittels vollständiger Induktion nach: Ist A eine (m, m)-Matrix (quadratisch!) und n \in \N \cup{0}, so gilt:

 

(E{m}-A) \sum{k=0}^n A ({k}=E_m-A){n+1} \quad \text { (geometrische Summenformel) }

 

Die Potenzen einer (m, m)-Matrix sind dabei auf die offensichtliche Weise erklärt:

 

A^0=E_m \quad(\text { Einheitsmatrix }), \quad A ({n+1}=A){n} A \quad \text { für } n \in \N \cup{0}

 

Hinweis: Sie dürfen hier alle aus der VL bekannten Rechenregeln im Zshg. mit Matrizen verwenden. Außerdem werden Sie noch eine weitere (in der VL noch nicht behandelte) benötigen, die man sich aber ganz leicht selbst überlegen kann (welche ist das wohl?). Abgesehen von der Nichtkommutativität rechnet es sich mit Matrizen (fast) genauso wie mit Zahlen, nur dass es halt keine Division gibt ...

 

 \setcounter{enumi}{28}

 

Bestimmen Sie zu jeder der folgenden Matrizen jeweils die Inverse oder weisen Sie nach, dass keine existiert.\

 

(a) (\begin{array}{ll}0 & 1 \ 1 & 0\end{array})\

 

(c) (\begin{array}{rrr}1 & 2 & 3 \ 0 & -4 & 5 \ 0 & 0 & 2\end{array})\

 

(e) (\begin{array}{rrrr}2 & 0 & 0 & 0 \ 1 & -4 & 0 & 0 \ 3 & 1 & 2 & 0 \ 4 & 3 & 2 & 1\end{array})\

 

(b) (\begin{array}{rrr}0 & 0 & 3 \ 0 & -4 & 0 \ 2 & 0 & 0\end{array})\

 

(d) (\begin{array}{lll}1 & 2 & 3 \ 0 & 0 & 5 \ 0 & 0 & 2\end{array})

 

Es sein hier n \in \N eine feste Zahl und D eine (n, n)-Diagonalmatrix (das heißt auf der Hauptdiagonalen von links oben nach rechts unten stehen irgendwelche Zahlen \lambda{1}, ..., \lambda{n}, sonst nur Nullen), ferner A eine invertierbare ( n, n )-Matrix und B sei definiert als B=A D A^{-1}.

 

(a) Ist A^{-1} invertierbar und wie sieht die Inverse ggf. aus?

 

(b) Geben Sie eine notwendige und hinreichende Bedingung für Invertierbarkeit von D an.

 

(c) Unter welcher Bedingung ist B invertierbar?

 

(d) Wie kann man B^k selbst für großes k sehr einfach und schnell ausrechnen?

 

 \setcounter{enumi}{30}

 

Eine (m, m)-Matrix (also quadratisch) A=(a_{k, r}) heißt strikt diagonal dominant, wenn gilt:

 

\sum{\substack{l=1 \ l \neq k}}^m|a{k, l}|<|a_{k, k}| \quad \text { für } k=1,2, ..., m \text {. }

 

Zu einer solchen strikt diagonal dominanten Matrix A bilde man nun die (m, m)-Matrix D= (d_{k, r}) mit

 

d_{k, r}={\begin{array}{cc}

 

a_{k, k}, & \text { falls } k=r \0, & \text { falls } k \neq r

 

\end{array}.

 

Weisen Sie nach:

 

(a) D ist invertierbar (dazu muss man die Inverse angeben).

 

(b) D^{-1} A ist wieder strikt diagonal dominant.

 

(c) B=E{m}-D^{-1} A (dabei ist E{m} die ( m, m )-Einheitsmatrix) hat auf der Hauptdiagonalen nur Nullen.

 

(d) Ist E_m-B invertierbar, so ist auch A invertierbar (dazu muss man die Inverse angeben).

 

Hintergrund: Für die Matrix B kann man in obiger Situation nachweisen, dass

 

\includegraphics[max width=\textwidth, center]{2024_04_08_7f221b5c3d546e64d663g-15}\

 

tatsächlich stets invertierbar ist { }^2, also auch A.

 

Eine quadratische strikt diagonal dominante Matrix ist also immer invertierbar. Normalerweise kann man einer Matrix ja nicht unmittelbar ansehen, ob sie invertierbar ist, es bedarf dazu meist einer recht aufwändigen Rechnung. Solche strikt diagonal dominante Matrizen tauchen z. B. als Koeffizientenmatrix eines LGS bei der sog. Spline-Interpolation { }^3 auf; man weiß daher, dass diese Spline-Konstruktion stets eindeutig möglich ist.

 

\footnotetext{{ }^2 sog. NEUMANNsche Reihe

 

{ }^3 vgl. Skript 14.2

 

}

 

 \setcounter{enumi}{31}

 

Berechnen Sie die folgenden Determinanten (falls möglich)!\

 

(a) |\begin{array}{rrr}2 & 1 & 0 \ 1 & 2 & -3 \ 2 & 1 & 4\end{array}|\

 

(b) |\begin{array}{rrr}9 & 2 & 1 \ 4 & 2 & -3 \ -2 & 3 & -23 \ 4 & 2 & -3\end{array}|\

 

(c) |\begin{array}{rrrr}2 & 1 & 0 & 3 \ 1 & 2 & -3 & 4 \ 2 & 1 & 4 & 1 \ -2 & 1 & -3 & 4\end{array}|

 

Berechnen Sie die folgenden Determinanten (falls möglich)!\

 

(a) |\begin{array}{rrrr}-2 & 1 & 4 & 3 \ 1 & 0 & -3 & 0 \ 3 & -5 & -7 & 2 \ 5 & 9 & 2 & 7\end{array}|\

 

(b) |\begin{array}{rrrrr}1 & 1 & 1 & 1 & 1 \ 1 & 2 & 4 & 8 & 16 \ 1 & -2 & 4 & -8 & 16 \ 1 & 3 & 9 & 27 & 81 \ 1 & -3 & 9 & -27 & 81\end{array}|

 

Bestimmen (nicht „Berechnen“) Sie die folgende Determinante!

 

|\begin{array}{rrrrrr}

 

-2 & 1 & 4 & 3 & 2 & 9 \1 & 0 & -3 & 0 & 7 & -2 \3 & -5 & -7 & 2 & 4 & -4 \5 & 9 & 2 & 7 & -3 & -1 \1 & -4 & -3 & 5 & 6 & 5 \1 & -2 & 4 & -4 & -1 & 2

 

\end{array}|

 

Hinweis: Sehen Sie sich die Zeilen genau an!

 

 \setcounter{enumi}{34}

 

Berechnen Sie die Lösungen der folgenden LGSe mit Hilfe der Cramerschen Regel!

 

(a) (\begin{array}{ll}9 & -3 \ 5 & -7\end{array})(\begin{array}{l}x{1} \ x{2}\end{array})=(\begin{array}{r}60 \ -36\end{array})

 

(b) (\begin{array}{rrr}3 & -1 & 2 \ -2 & 1 & 1 \ 4 & -3 & -4\end{array})(\begin{array}{l}x{1} \ x{2} \ x_3\end{array})=(\begin{array}{r}-7 \ 11 \ -31\end{array})

 

 \setcounter{enumi}{35}

 

Berechnen Sie die Lösung des folgenden LGS mit Hilfe der CRAMERschen Regel!

 

(\begin{array}{rrrr}

 

2 & -2 & 1 & -3 \2 & 1 & 3 & 1 \-2 & 2 & -3 & 2 \2 & -2 & -1 & 4

 

\end{array})(\begin{array}{l}

 

x{1} \x{2} \x{3} \x{4}

 

\end{array})=(\begin{array}{r}

 

9 \20 \-17 \17

 

\end{array})

 

 \setcounter{enumi}{36}

 

Zwei ( n, n)-Matrizen A, B heißen ähnlich (geschrieben A \sim B, Reihenfolge beachten!), wenn es eine invertierbare (n, n)-Matrix X gibt, so dass B=X^{-1} A X ist. (Ein gewisser Zusammenhang mit Aufgabe 30 ist nicht ganz zufällig ...)

 

Zeigen Sie: Die Ähnlichkeit von Matrizen ist eine Äquivalenzrelation, das heißt

 

(a) Für jede (n, n)-Matrix A ist A \sim A (Reflexivität).

 

(b) Für zwei beliebige ( n, n )-Matrizen A, B ist A \sim B äquivalent zu B \sim A (Symmetrie).

 

(c) Gilt für irgendwelche (n, n)-Matrizen A, B, C zum einen A \sim B, zum anderen auch B \sim C, so folgt A \sim C (Transitivität).

 

Hinweis: Bei allen drei Teilen muss die benötigte Matrix X irgendwie angegeben und ihre Invertierbarkeit begründet werden. Bei einer Äquivalenz gibt es immer zwei „Richtungen“, die nachzuweisen sind.

 

 \setcounter{enumi}{37}

 

Für eine ( n, n )-Matrix B heißt \operatorname{ker}(B)={\vec{v} \in \C^n \mid B \vec{v}=\vec{0}} der Kern der Matrix (oder ihrer zugehörigen linearen Abbildung). Für solch eine quadratische Matrix kann man natürlich auch via B^0=E_n, B ({k+1}=B){k} B, Potenzen berechen. Weisen Sie nach

 

(a) Für k=0,1,2, ... ist \operatorname{ker}(B^k) stets ein UVR des \C^n (was ist dabei wohl \operatorname{ker}(B^0) ?).

 

(b) Für k=0,1,2, ... ist stets \operatorname{ker}(B^k) \subseteq \operatorname{ker}(B^{k+1}).\

 

39. B sei eine (n, n)-Matrix, \vec{u} \in \C^n irgendein Vektor. Damit berechne man für k=0,1,2, ... die Vektoren \vec{v}{k}=B^k \vec{u}. Weisen Sie nach: Ist \vec{v}{m} \neq \vec{0}, aber \vec{v}{m+1}=\vec{0}, so ist die Menge {\vec{v}{0}, \vec{v}{1}, \vec{v}{2}, ..., \vec{v}_m} linear unabhängig.

 

 \setcounter{enumi}{39}

 

Bestimmen Sie alle Eigenwerte und jeweils einen zugehörigen Eigenvektor der Matrix

 

(\begin{array}{rr}

 

3 & -1 \1 & 5

 

\end{array})

 

 \setcounter{enumi}{40}

 

Bestimmen Sie alle Eigenwerte und jeweils einen zugehörigen Eigenvektor der Matrix

 

(\begin{array}{rrr}

 

3 & -1 & -1 \1 & 5 & -1 \-2 & -2 & 4

 

\end{array})

 

 \setcounter{enumi}{41}

 

Bestimmen Sie alle Eigenwerte und jeweils einen zugehörigen Eigenvektor der Matrix

 

(\begin{array}{rrr}

 

3 & 1 & -1 \2 & 4 & 2 \1 & 1 & 5

 

\end{array})

 

 \setcounter{enumi}{42}

 

Bestimmen Sie alle Eigenwerte und jeweils einen zugehörigen Eigenvektor der Matrix

 

(\begin{array}{rrr}

 

4 & -2 & 2 \-1 & 5 & -1 \1 & 1 & 3

 

\end{array})

 

 \setcounter{enumi}{43}

 

Bestimmen Sie alle Eigenwerte und jeweils einen zugehörigen Eigenvektor der Matrix

 

(\begin{array}{rrrr}

 

2 & 8 & -2 & 2 \6 & 4 & -2 & 2 \3 & -2 & -1 & -7 \-3 & 2 & -7 & -1

 

\end{array})

 

Hinweis: Die Eigenwerte sind allesamt unter \pm 1, \pm 2, \pm 3, ..., \pm 9 zu finden.

 

 \setcounter{enumi}{44}

 

Zeigen Sie: Sind A, B ähnliche (n,n)-Matrizen (s. Aufgabe 37), so gilt

 

\operatorname{det}(A-\lambda E{n})=\operatorname{det}(B-\lambda E{n}) \quad \text { für alle } \lambda \in \C \text {. }

 

Hinweis: Der Beweis ist zwar recht kurz, aber man benötigt so ziemlich jede der Rechenregeln für Matrizen, inverse Matrizen und Determinanten, dies ist also ein Rundumschlag durch die Matrizenrechnung.

 

 \setcounter{enumi}{45}

 

Angenommen, eine (n,n)-Matrix A ist ähnlich (s. Aufgabe 37) zu einer (n, n) Diagonalmatrix D, das heißt D=X^{-1} A X. Was kann man dann über die EWe und EVen der \overline{\text { Matrix } A} beziehungsweise über die Matrix X aussagen?

 

Überprüfen Sie die Matrizen aus den Eigenwertaufgaben vom vorigen Übungsblatt, ob sie zu einer Diagonalmatrix ähnlich sind! Die vorige Aufgabe ist dabei ganz nützlich!

 

Überprüfen Sie außerdem noch die Matrix

 

(\begin{array}{rrr}

 

4 & 1 & -1 \-2 & 1 & 1 \0 & 0 & 2

 

\end{array})

 

auf diese sog. „Diagonalisierbarkeit“!

 

 \setcounter{enumi}{47}

 

Eine Blockdiagonalmatrix ist eine quadratische Matrix der Form

 

(\begin{array}{cccccc}

 

A{1} & 0 & & & \cdots & 0 \0 & A{2} & 0 & & \cdots & 0 \0 & 0 & A{3} & 0 & \cdots & 0 \\vdots & \vdots & \ddots & \ddots & \ddots & \vdots \0 & \cdots & & 0 & A{m-1} & 0 \0 & \cdots & & & 0 & A_m

 

\end{array}),

 

wobei A{1}, A{2}, ..., A_m nicht Zahlen, sondern wiederum quadratische Matrizen (beliebiger Größe) sind (und die Nullen natürlich für Nullmatrizen passender Größe stehen).

 

Überlegen Sie, wie man die Determinante einer solchen (selbst sehr großen) Blockdiagonalmatrix leicht ausrechnen kann, sofern die einzelnen Blöcke (A_1, ...) nicht allzu groß sind. Wie verhält es sich mit EWen und EVen?

 

Tipp: Was passiert wohl, wenn man zwei solcher Blockdiagonalmatrizen A, B, wo die sich entsprechenden Blöcke von A und B jeweils gleich groß sind, miteinander multipliziert?

 

 \setcounter{enumi}{48}

 

Gegeben seien die Vektoren \vec{a}=(\begin{array}{r}3 \ -2 \ 1\end{array}), \vec{b}=(\begin{array}{r}0 \ -1 \ 7\end{array}), \vec{c}=(\begin{array}{r}-1 \ 1 \ -1\end{array}), \vec{d}=(\begin{array}{l}1 \ 2 \ 1\end{array}).

 

(a) Berechnen Sie

 

3 \vec{a}+2 \vec{b}-(\vec{a} \cdot \vec{b}) \vec{c}+\vec{d} \quad \text { und } \quad-2(\vec{b}+5 \vec{c})-3(\vec{a} \cdot \vec{d})(\vec{a}+\vec{b})

 

(b) Normieren Sie die Vektoren \vec{a}, \vec{b}, \vec{c} und \vec{d}.

 

(c) Welchen Winkel schließen \vec{a} und \vec{b} beziehungsweise \vec{c} und \vec{d} miteinander ein?

 

(d) Welche der Vektoren \vec{a}, \vec{b}, \vec{c} und \vec{d} sind orthogonal?

 

(e) Warum ist die Schreibweise ,, \vec{a} \cdot \vec{b} \cdot \vec{c} “ ausgesprochen gefährlich?

 

(Muss das in einer bestimmten Reihenfolge ausgerechnet werden, oder ist die Reihenfolge — wie bei der normalen Multiplikation reeller Zahlen — egal? Nachrechnen!)

 

 \setcounter{enumi}{49}

 

Gegeben seien die Vektoren aus Aufgabe 49.

 

(a) Berechnen Sie

 

\vec{a} \cdot(\vec{b} \times \vec{c}) \quad \text { und } \quad \vec{b} \times(\vec{b}+\vec{c})

 

(b) Warum ist die Schreibweise ,, \vec{a} \times \vec{b} \times \vec{c} “ ausgesprochen gefährlich?

 

(Muss das in einer bestimmten Reihenfolge ausgerechnet werden, oder ist die Reihenfolge - wie bei der normalen Multiplikation reeller Zahlen - egal? Nachrechnen!)

 

 \setcounter{enumi}{50}

 

Normieren sie die Vektoren \vec{a}=(\begin{array}{r}1 \ 2 \ -1\end{array}), \vec{b}=(\begin{array}{r}3 \ -1 \ 1\end{array}) und \vec{c}=(\begin{array}{r}-1 \ 4 \ 7\end{array}).

 

Bilden die normierten Vektoren ein orthonormiertes System?

 

Berechnen Sie ferner\

 

(a) \vec{a} \times(3 \vec{b}+5 \vec{c})\

 

(b) \vec{b} \times(2 \vec{a}-7 \vec{c})\

 

(c) \vec{c} \times(\vec{a}+\vec{b})

 

Hinweis: Ein orthonormiertes System ist eine Menge von Einheitsvektoren, die paarweise orthogonal sind.

 

 \setcounter{enumi}{51}

 

Gegeben sei ein Vektor \vec{x} und eine „Richtung“ \vec{a} mit |\vec{a}|=1 (da \vec{a} tatsächlich nur eine Richtung im Raum angeben soll, ist der Betrag von \vec{a} im Prinzip beliebig, man vereinfacht sich die folgenden Schreibarbeiten jedoch, wenn man gleich einen Einheitsvektor nimmt).

 

(a) Schreiben Sie \vec{x} in der Form \vec{x}=\vec{x}{\vec{a}}+\vec{r}, so dass \vec{x}{\vec{a}} (anti-) parallel zu \vec{a} ist und andererseits \vec{r}, \vec{a} orthogonal sind.

 

Diese Zerlegung eines Vektor in eine Summe, deren einer Summand parallel, der andere orthogonal zu einem vorgegebenem Vektor ist, nennt man orthogonale Zerlegung.

 

(b) Bestimmen Sie zu \vec{r} einen Vektor \vec{r}{\perp}, der denselben Betrag wie \vec{r} hat, so dass \vec{a}, \vec{r}, \vec{r}{\perp} paarweise orthogonal sind.

 

(c) Bestimmen Sie den Vektor \vec{r}_{\varphi}, der durch Drehung von \vec{r} um den Winkel \varphi bezüglich der Drehachse \vec{a} entsteht.

 

(d) Bestimmen Sie den Vektor \vec{x}_{\varphi}, der durch Drehung von \vec{x} um den Winkel \varphi bezüglich der Drehachse \vec{a} entsteht.

 

 \setcounter{enumi}{52}

 

Weisen Sie nach:

 

(a) Wird ein (ebenes) Parallelogramm durch die Vektoren \vec{a}=(\begin{array}{ll}a{x} & a{y}\end{array})^{\mathrm{T}}, \vec{b}= (\begin{array}{ll}b{x} & b{y}\end{array})^{\mathrm{T}} aufgespannt, so ist sein Flächeninhalt der Betrag der (2,2)-Determinante mit den Spalten \vec{a} und \vec{b}.

 

(b) Wird ein (räumlicher) Spat durch die Vektoren \vec{a}=(\begin{array}{lll}a{x} & a{y} & a{z}\end{array})^{\mathrm{T}}, \vec{b}= (\begin{array}{lll}b{x} & b{y} & b{z}\end{array})^{\mathrm{T}} und \vec{c}=(\begin{array}{lll}c{x} & c{y} & c_z\end{array})^{\mathrm{T}} aufgespannt, so ist sein Volumen der Betrag der (3,3)-Determinante mit den Spalten \vec{a}, \vec{b} und \vec{c}.

 

 \setcounter{enumi}{53}

 

Weisen Sie folgende Rechenregeln für das sog. Spatprodukt im \R^3,\langle\vec{a}, \vec{b}, \vec{c}\rangle=\vec{a} \cdot(\vec{b} \times \vec{c}), nach!

 

(a) \langle\vec{a}, \vec{b}, \vec{c}\rangle=-\langle\vec{b}, \vec{a}, \vec{c}\rangle=-\langle\vec{c}, \vec{b}, \vec{a}\rangle=-\langle\vec{a}, \vec{c}, \vec{b}\rangle

 

(Vorzeichenwechsel bei Vertauschung zweier Faktoren)

 

(b) \langle\vec{a}, \vec{b}, \vec{c}\rangle=\langle\vec{b}, \vec{c}, \vec{a}\rangle=\langle\vec{c}, \vec{a}, \vec{b}\rangle (zyklische Vertauschbarkeit)

 

jeweils für beliebige Vektoren \vec{a}, \vec{b}, \vec{c} \in \R^3={(\begin{array}{l}x \ y \ z\end{array}): x, y, z \in \R}.

 

 \setcounter{enumi}{54}

 

Weisen Sie die sog. GRASSMANN- beziehungsweise LAGRANGE-Identität im \R^3 nach!

 

(a) \vec{a} \times(\vec{b} \times \vec{c})=(\vec{a} \cdot \vec{c}) \vec{b}-(\vec{a} \cdot \vec{b}) \vec{c}

 

(b) (\vec{a} \times \vec{b}) \cdot(\vec{c} \times \vec{d})=(\vec{a} \cdot \vec{c})(\vec{b} \cdot \vec{d})-(\vec{a} \cdot \vec{d})(\vec{b} \cdot \vec{c})\

 

56. A sei hier eine (n, n)-Matrix. Weisen Sie nach:

 

(a) Ist L{k} eine (n, n)-Eliminationsmatrix (vgl. Skript), so entsteht L{k} A aus A durch Addition des l{k+1, k}-fachen der k-ten Zeile zur (k+1)-ten Zeile, des l{k+2, k}-fachen der k-ten Zeile zur (k+2)-ten Zeile, ..., des l_{n, k}-fachen der k-ten Zeile zur n-ten Zeile.

 

(b) Ist L{k} eine Eliminationsmatrix, so entsteht A L{k}^{\mathrm{T}} aus A durch Addition des l{k+1, k}-fachen der k-ten Spalte zur (k+1)-ten Spalte, des l{k+2, k}-fachen der k-ten S p a l t e ~ z u r ~(k+2)-ten Spalte, ..., des l_{n, k}-fachen der k-ten Spalte zur n-ten Spalte.

 

(c) Ist P eine (n, n) Permutationsmatrix (vgl. Skript), bei der genau die Elemente (1, r{1}), (2, r{2}), ...,(n, r{n}) gleich 1 sind, so ist P A die (n, n)-Matrix, deren k-te Zeile die r{k}-te Zeile von A ist.

 

(d) Ist P eine (n, n) Permutationsmatrix, bei der genau die Elemente (k{1}, 1),(k{2}, 2), ..., (k{n}, n) gleich 1 sind, so ist A P die (n, n)-Matrix, deren r-te Spalte die k{r}-te Spalte von A ist.

 

 \setcounter{enumi}{56}

 

Bestimmen Sie die Lösung(en) der folgenden LGSe mittels L R-Zerlegung.

 

(a) (\begin{array}{rrrr|r}-3 & 2 & & -1 & -20 \ 9 & -3 & -2 & 4 & 42 \ -12 & 14 & -6 & 3 & -115 \ 15 & -4 & 2 & -6 & 63\end{array})

 

(b) (\begin{array}{rrrrr|r}4 & -6 & 2 & 1 & 1 & 36 \ -6 & 9 & -4 & -1 & 2 & -69 \ 2 & -3 & 2 & 0 & -1 & 23 \ -4 & 9 & -6 & 1 & 6 & -49 \ -2 & 9 & -12 & 13 & 16 & 39\end{array})

 

 \setcounter{enumi}{57}

 

Bestimmen Sie mittels L R-Zerlegung die Lösung(en) der folgenden LGSe:

 

(a) (\begin{array}{rrrrrrr|r}0 & 1 & -1 & 1 & 1 & 0 & -1 & 1 \ 1 & 0 & 1 & 1 & -1 & -1 & 1 & 4 \ 0 & 1 & 0 & 1 & 0 & 1 & 0 & 12 \ -1 & 0 & -1 & 0 & -1 & 0 & -1 & -16 \ 1 & 1 & 1 & 1 & 1 & 1 & -1 & 14 \ 1 & -1 & 1 & 0 & 0 & 0 & 0 & 2 \ 0 & 0 & 0 & 1 & 1 & -1 & 1 & 10\end{array})\

 

(b) (\begin{array}{rrrrrrr|r}0 & 1 & -1 & 1 & 1 & 0 & -1 & -3 \ 1 & 0 & 1 & 1 & -1 & -1 & 1 & 2 \ 0 & 1 & 0 & 1 & 0 & 1 & 0 & -3 \ -1 & 0 & -1 & 0 & -1 & 0 & -1 & -4 \ 1 & 1 & 1 & 1 & 1 & 1 & -1 & -1 \ 1 & -1 & 1 & 0 & 0 & 0 & 0 & 3 \ 0 & 0 & 0 & 1 & 1 & -1 & 1 & 2\end{array})

 

Um Rechenfehler auszuschließen, überprüfen Sie bitte, dass das Produkt von L und R (abgesehen von eventuell nötigen Zeilenvertauschungen) tatsächlich die ursprüngliche Koeffizientenmatrix ergibt. Falls nein, muss man wohl oder übel noch einmal von vorn beginnen

 

 \setcounter{enumi}{58}

 

Gegeben seien ein fester Vektor \vec{a}=(\begin{array}{lll}a{x} & a{y} & a_z\end{array})^{\mathrm{T}} \in \R^3 und drei Zahlen \alpha, \beta, \gamma \in \R. Bestimmen Sie (3, 3)-Matrizen A, B und C, so dass

 

(a) A \vec{x}=\alpha \vec{x} für alle \vec{x} \in \R^3,

 

(b) B \vec{x}=\beta(\vec{x} \cdot \vec{a}) \vec{a} für alle \vec{x} \in \R^3 und

 

(c) C \vec{x}=\gamma(\vec{x} \times \vec{a}) für alle \vec{x} \in \R^3 gilt.

 

Gibt es wohl ein einfaches „Rezept“, wie man diese Matrizen bestimmen kann?

 

 \setcounter{enumi}{59}

 

Ist \vec{a} \in \R^3 ein Einheitsvektor und \varphi irgendein Winkel, so kann man zu x \in \R^3 den um \varphi bezüglich der Drehachse \vec{a} gedrehten Vektor \vec{x}_{\varphi} bekanntlich wie folgt berechnen (vgl. Aufgabe 52):

 

\begin{itemize}

 

Orthogonale Zerlegung \vec{x}=\vec{x}{\vec{a}}+\vec{r} mit \vec{x}{\vec{a}}=(\vec{x} \cdot \vec{a}) \vec{a}, \vec{r}=\vec{x}-(\vec{x} \cdot \vec{a}) \vec{a}

 

Bestimmung des bezüglich \vec{a} um 90^{\circ} gedrehten Vektors \vec{r}{\perp} mit \vec{r}{\perp}=\vec{r} \times \vec{a}

 

Drehung von \vec{r} um den Winkel \varphi bezüglich \vec{a}: \vec{r}{\varphi}=\cos \varphi \cdot \vec{r}+\sin \varphi \cdot \vec{r}{\perp}

 

Drehung von \vec{x} um den Winkel \varphi bezüglich \vec{a}: \vec{x}{\varphi}=\vec{x}{\vec{a}}+\vec{r}_{\varphi}

 

\end{itemize}

 

Daraus ergibt sich (Nachrechnen!) \vec{x}_{\varphi}=\cos \varphi \cdot \vec{x}+(1-\cos \varphi)(\vec{x} \cdot \vec{a}) \vec{a}+\sin \varphi \cdot(\vec{x} \times \vec{a}).

 

(a) Bestimmen Sie eine (3,3)-Matrix D{\varphi}, so dass \vec{x}{\varphi}=D_{\varphi} \vec{x} für alle \vec{x} \in \R^3 gilt.

 

(b) Berechnen Sie \operatorname{det}(D_{\varphi}). Zum direkten Ausrechnen dieser Determinante braucht man gute Nerven und viel Papier. Mit Hilfe von Aufgabe 53b ist das aber in einer Zeile erledigt.

 

(c) Ist D_{\varphi} invertierbar? Wie sieht die Inverse ggf. aus?

 

 \setcounter{enumi}{60}

 

Berechnen Sie den Abstand des Punktes Q zur Geraden g !

 

Q=(\begin{array}{r}

 

8 \-8 \24

 

\end{array}), \quad g:(\begin{array}{r}

 

1 \2 \-3

 

\end{array})+\lambda(\begin{array}{r}

 

3 \-2 \7

 

\end{array})

 

 \setcounter{enumi}{61}

 

Berechnen Sie den Abstand des Punktes Q zur Geraden g !

 

Q=(\begin{array}{r}

 

12 \3 \11

 

\end{array}), \quad g:(\begin{array}{r}

 

-2 \1 \3

 

\end{array})+\lambda(\begin{array}{r}

 

7 \-1 \4

 

\end{array})

 

 \setcounter{enumi}{62}

 

Berechnen Sie den Abstand und ggf. Schnittpunkt und Schnittwinkel der beiden angegebenen Geraden g{1} und g{2} !

 

g_1:(\begin{array}{r}

 

-1 \2 \5

 

\end{array})+\lambda(\begin{array}{l}

 

9 \1 \2

 

\end{array}), \quad g_2:(\begin{array}{l}

 

2 \2 \5

 

\end{array})+\mu(\begin{array}{r}

 

7 \-1 \-1

 

\end{array})

 

 \setcounter{enumi}{63}

 

Berechnen Sie den Abstand und ggf. Schnittpunkt und Schnittwinkel der beiden gegebenen Geraden g{1} und g{2} !

 

g_1:(\begin{array}{r}

 

-3 \-5 \0

 

\end{array})+\lambda(\begin{array}{r}

 

-3 \-2 \2

 

\end{array}), \quad g_2:(\begin{array}{r}

 

0 \-30 \-39

 

\end{array})+\mu(\begin{array}{r}

 

2 \9 \12

 

\end{array})

 

 \setcounter{enumi}{64}

 

Berechnen Sie den Abstand und ggf. Schnittpunkt und Schnittwinkel der beiden gegebenen Geraden g{1} und g{2} !

 

g_1:(\begin{array}{r}

 

-2 \8 \12

 

\end{array})+\lambda(\begin{array}{r}

 

-1 \2 \3

 

\end{array}), \quad g_2:(\begin{array}{r}

 

7 \-9 \-6

 

\end{array})+\mu(\begin{array}{r}

 

-3 \3 \4

 

\end{array})

 

 \setcounter{enumi}{65}

 

Berechnen Sie den Abstand und ggf. Schnittpunkt und Schnittwinkel der beiden gegebenen Geraden g{1} und g{2} !

 

g_1:(\begin{array}{r}

 

-1 \-8 \-12

 

\end{array})+\lambda(\begin{array}{l}

 

1 \2 \5

 

\end{array}), \quad g_2:(\begin{array}{l}

 

8 \2 \5

 

\end{array})+\mu(\begin{array}{l}

 

3 \2 \1

 

\end{array})

 

 \setcounter{enumi}{66}

 

Berechnen Sie den Abstand des Punktes Q zur Geraden g und zur Ebene E.

 

Q=(\begin{array}{l}

 

3 \5 \1

 

\end{array}), g:(\begin{array}{r}

 

-5 \2 \1

 

\end{array})+\lambda(\begin{array}{r}

 

-1 \2 \3

 

\end{array}), \quad E:(\begin{array}{r}

 

2 \-4 \3

 

\end{array})+\mu(\begin{array}{r}

 

-2 \3 \5

 

\end{array})+\nu(\begin{array}{r}

 

-3 \2 \2

 

\end{array})

 

 \setcounter{enumi}{67}

 

Berechnen Sie den Abstand des Punktes Q zur Geraden g und zur Ebene E.

 

Q=(\begin{array}{l}

 

1 \5 \3

 

\end{array}), g:(\begin{array}{r}

 

-5 \-2 \1

 

\end{array})+\lambda(\begin{array}{r}

 

-1 \-2 \3

 

\end{array}), \quad E:(\begin{array}{r}

 

2 \-4 \3

 

\end{array})+\mu(\begin{array}{r}

 

2 \3 \-5

 

\end{array})+\nu(\begin{array}{r}

 

3 \2 \-2

 

\end{array})

 

 \setcounter{enumi}{68}

 

Berechnen Sie den Abstand der Geraden g zur Ebene E und ggf. Schnittpunkt und Schnittwinkel!

 

g:(\begin{array}{r}

 

1 \-2 \5

 

\end{array})+\lambda(\begin{array}{l}

 

1 \2 \3

 

\end{array}), \quad E:(\begin{array}{l}

 

2 \1 \3

 

\end{array})+\lambda(\begin{array}{r}

 

3 \-2 \-4

 

\end{array})+\mu(\begin{array}{l}

 

1 \0 \2

 

\end{array})

 

 \setcounter{enumi}{69}

 

Berechnen Sie den Abstand der Geraden g zur Ebene E und ggf. Schnittpunkt und Schnittwinkel!

 

g:(\begin{array}{r}

 

3 \2 \-1

 

\end{array})+\lambda(\begin{array}{l}

 

3 \1 \6

 

\end{array}), \quad E:(\begin{array}{r}

 

-2 \7 \-5

 

\end{array})+\lambda(\begin{array}{l}

 

9 \3 \2

 

\end{array})+\mu(\begin{array}{r}

 

-2 \0 \-1

 

\end{array})

 

 \setcounter{enumi}{70}

 

Bestimmen Sie jeweils zu der Ebene E eine Darstellung in Punkt-Richtungs-Form beziehungsweise Parameterform!\

 

(a) E:(\vec{p}-(\begin{array}{r}4 \ 3 \ -2\end{array})) \cdot(\begin{array}{r}3 \ 0 \ -7\end{array})=0 \quad (b) E:(\vec{p}-(\begin{array}{r}1 \ -3 \ -4\end{array})) \cdot(\begin{array}{l}2 \ 5 \ 3\end{array})=0

 

Bestimmen Sie eine Normalenform derjenigen Ebene E, die die Gerade

 

g:(\begin{array}{r}

 

7 \2 \-3

 

\end{array})+\lambda(\begin{array}{l}

 

2 \4 \1

 

\end{array}) \text { enthält und parallel zur Geraden } h:(\begin{array}{l}

 

11 \13 \15

 

\end{array})+\lambda(\begin{array}{l}

 

3 \3 \6

 

\end{array})

 

ist. Geben Sie die Ebene E mit einem normierten Normalenvektor an!

 

 \setcounter{enumi}{72}

 

Berechnen Sie den Abstand der beiden Ebenen E{1}, E{2} und bestimmen Sie ggf. Schnittgerade und Schnittwinkel!

 

E_1:(\begin{array}{r}

 

-2 \7 \-5

 

\end{array})+\lambda(\begin{array}{l}

 

9 \3 \2

 

\end{array})+\mu(\begin{array}{r}

 

-2 \0 \-1

 

\end{array}), \quad E_2:(\begin{array}{l}

 

1 \1 \1

 

\end{array})+\lambda(\begin{array}{l}

 

2 \3 \4

 

\end{array})+\mu(\begin{array}{r}

 

2 \-4 \3

 

\end{array})

 

 \setcounter{enumi}{73}

 

Berechnen Sie den Abstand der beiden Ebenen E{1}, E{2} und bestimmen Sie ggf. Schnittgerade und Schnittwinkel!

 

E_1:(\begin{array}{r}

 

5 \-2 \2

 

\end{array})+\lambda(\begin{array}{r}

 

3 \1 \-5

 

\end{array})+\mu(\begin{array}{r}

 

2 \-4 \-1

 

\end{array}), \quad E_2:(\begin{array}{r}

 

2 \-4 \3

 

\end{array})+\lambda(\begin{array}{r}

 

2 \3 \-5

 

\end{array})+\mu(\begin{array}{r}

 

3 \2 \-2

 

\end{array})

 

\end{document}