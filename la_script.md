## 1 Ein Körper hat keine sog. Nullteiler (Definition und Nachweis finden Sie im Skript, S. 110f).
Weisen Sie für die Menge der komplexe Zahlen $\mathbb{C}$ nach, dass  
(a) es mit der Multiplikation wie aus der VL keine Nullteiler gibt, während  
(b) es bei der „naiven“ Definition einer Multiplikation gemäß (x1 + y1 j) · (x2 + y2 j) = (x1 · x2) + (y1 · y2) j jedoch Nullteiler gäbe.  


Die scheinbar komplizierte Definition der Multiplikation komplexer Zahlen wie in der VL ist
also nötig . . .
Beim ersten Teil kann man entweder direkt vorgehen (mühsam und unübersichtlich) oder
man lässt sich vom allgemeinen Beweis der Nullteilerfreiheit bei Körpern (s. Skript) inspi-
rieren. Natürlich dürfen Sie diesen Satz selbst nicht verwenden, denn Sie sollen das gerade
für diesen Spezialfall „zu Fuß“ nachweisen. Was in der Definition eines Körpers ausdrücklich
steht (sprich: Gruppe bzgl. Addition bzw. — ohne die Null — bzgl. Multiplikation und das
Distributivgesetz) dürfen Sie jedoch verwenden.
Beim zweiten Teil sollte man ein konkretes Beispiel angeben.

## 2 Zur Wiederholung . . . Weisen Sie nach:
(a) $\mathbb{Z}_7$ = {0, 1, 2, . . . , 6} versehen mit der üblichen Addition und Multiplikation, allerdings
modulo 7, ist ein Körper. Geben Sie dabei auch an, wie die neutralen und inversen Ele-
mente aussehen.  
(b) $\mathbb{Z}_8$ = {0, 1, 2, . . . , 7} versehen mit der üblichen Addition und Multiplikation, allerdings
modulo 8, ist kein Körper.
Da die Mengen hier doch noch recht übersichtlich sind, ist es wohl am einfachsten, die in-
versen Elemente (so vorhanden) konkret anzugeben. Etwas unangenehm wird allerdings die
Assoziativität, denn da sind ja drei Elemente beteiligt, d. h. es gibt bereits $7^3$ bzw. $8^3$ verschie-
dene Möglichkeiten. Deshalb sollte man sich hier überlegen, was diese Modulo-Rechnung
eigentlich bewirkt, und nicht etwa alle Möglichkeiten abklappern . . .

## 3 Ein naheliegender Grund für die „Erfindung“ komplexer Zahlen ist, dass man quadratische Gleichungen gern ohne Einschränkung lösen können möchte:

Berechnen Sie die Lösungen der folgenden quadratischen Gleichungen, z. B. mit der wohl-
bekannten $p-q-$Formel. Wenn etwas Negatives unter der Wurzel herauskommt, muss man an
$j^2$ = −1 denken und etwas improvisieren. Damit Sie sich sicher sind, dass das wirklich er-
folgreich war, machen Sie bitte in jedem Fall die Einsetzprobe!  
(a) $x^2 + 16 = 0$  
(b) $x^2 + 6x + 13 = 0$  
(c) $4x^2 + 16x + 80 = 0$

## 3 Weisen Sie nach: Ist V ein Vektorraum über einem Körper $\mathbb{K}$ (Achtung, nicht notwendig $\mathbb{R}$ oder $\mathbb{C}$, sondern ein beliebiger Körper!), so gelten  
(a) 0 $\odot \overrightarrow{v} = 0 für alle v $/in$ V  
(b) $\lambda \odot \overrightarrow{0}$ = 0 für alle $\lambda \in \mathbb{K}$  

Das mag ziemlich selbstverständlich aussehen, ist aber dennoch beweiswürdig. Sie dürfen
dabei über den Körper $\mathbb{K}$ und den Vektorraum V außer den in den Definitionen genannten
Eigenschaften und den bekannten Sätzen (Skript S. 100-102) nichts voraussetzen.  

Wenn man etwa an den $\mathbb{R}^3$ über $\mathbb{R}$ denkt, sind beide Aussagen natürlich offensichtlich, Sie
sollen diese aber eben nicht für ein konkretes Beispiel nachprüfen, sondern sich vergewissern,
dass diese tatsächlich allgemeingültig sind! Und für irgendeinen abstrakten Vektorraum ist das
halt nicht so ganz offensichtlich.  
nsbesondere: 0 ist nicht notwendig die bekannte Zahl 0, sondern „nur“ das neutrale Element
bzgl. der Addition in $\mathbb{K}$, entsprechend⃗ 0 „nur“ das neutrale Element bzgl. der Adddition in V . 
Der Deutlichkeit halber benutzen Sie bitte für die Addition in V und die Multiplikation mit
einem Skalar durchgängig $"\oplus, \odot"$ im Ggs. zu „+, ·“ für die Operationen in $\mathbb{K}$.
Hilfreich für diese Beweise sind insbesondere die beiden Distributivgesetze in der Definition
eines Vektorraumes . . .  

Bei den folgenden Aufgaben (und generell für die Zukunft) können wir auf die Unterscheidung
von "$\oplus, \odot$" für die Operationen auf V im Ggs. zu „+, ·“ für die Operationen in K verzichten,
denn dies ist aus dem Zusammenhang klar. Auch wird "$\odot$" meist ganz weggelassen.

## 4 Weisen Sie mittels vollständiger Induktion nach:
Ist U ein UVR eines Vektorraumes (V, $\mathbb{K}$, +, ·), so gilt für jedes n $\in \mathbb{N}$ = {1, 2, 3, . . .}:
Sind $\lambda_1, \lambda_2, . . . , \lambda_n \in \mathbb{K},\overrightarrow{u}_1, \overrightarrow{u}_2, . . . , 
\overrightarrow{u}_n \in U , so ist auch
\sum^{n}_{k=1} \lambda_k \overrightarrow{u}_k \in U.$  

Es wird hier ein auch formal korrekter Induktionsbeweis erwartet! Beachten Sie bitte, dass in
der Definition eines UVRes diese Aussage nur für „n = 2“ verlangt wird. Die Fälle „n = 1“
und "$n \leq 2$" müssen also getrennt bearbeitet werden.

## 5 Weisen Sie nach: Ist U ein UVR des Vektorraumes (V, K, +, ·), so ist auch (U, K, +, ·) ein Vektorraum.
**Hinweis**: Das sieht ja vielleicht völlig offensichtlich aus, aber hier sollen tatsächlich alle 
geforderten Eigenschaften eines Vektorraumes einzeln überprüft werden! Denken Sie bitte
insbesondere daran, dass die inverse Elemente bzgl. „+“ von den Elementen aus U ja in U
liegen müss(t)en.  

Entscheiden Sie jeweils, ob U ein UVR von V ist (Nachweis!):  
(a) V = $\mathbb{R}^4  U = {\begin{pmatrix}
x_1 \\
x_2 \\
x_3 \\
x_4 \\
\end{pmatrix} \in \mathbb{R}^4 | x_1 2x_2 +3x_3 = 0}$  
(b) V = $\mathbb{R}^3  U = {\begin{pmatrix}
x_1 \\
x_2 \\
x_3 \\
\end{pmatrix} \in \mathbb{R}^3 | x_1 2x_2= 1}$  
(c) V = $\mathbb{R}^2  U = {\begin{pmatrix}
x_1 \\
x_2 \\
\end{pmatrix} \in \mathbb{R}^2 | x_1*x_2= 0}$  
(d) V = $\mathbb{R}^4  U = {\begin{pmatrix}
x_1 \\
x_2 \\
x_3 \\
x_4 \\
\end{pmatrix} \in \mathbb{R}^4 | |x_1|=|X_2|}$  
das "$|x_1|$" steht für den sog. betrag einer reellen Zahl:  
$|x|=\left\{ \begin{matrix}
+x, & falls x \geq 0 \\
-x, & falls x < 0 
\end{matrix}\right.$
Z.B. ist |-3| = +3 und |+4|= +4  
(e) V = $\mathbb{R}^3  U = {\begin{pmatrix}
x_1 \\
x_2 \\
x_3
\end{pmatrix} \in \mathbb{R}^3 | x_1 = x_2}$  
(f) V = C(|0,1|), U = ${f \in V | f(0)=0=f(1)}$  
Mit C([ 0, 1 ]) ist die Menge (und Vektorraum!) der stetigen Funktionen auf dem In-
tervall [ 0, 1 ] gemeint. Stetig heißt anschaulich: Man kann den Funktionsgraphen der
Funktion (über das Intervall von 0 bis 1) in „einem Zug“ ohne Absetzen zeichnen.
Für diese Aufgabe ist das nicht weiter von Bedeutung, man könnte auch den Vektorraum
der (beliebigen) auf dem Intervall [ 0, 1 ] definierten Funktionen betrachten. Nur möchte
man meistens mit den Vektoren (hier Funktionen) noch irgend etwas „anfangen“, und
sei es nur, den Funktionsgraphen zu zeichnen, und Funktionen ohne jede Einschränkung
können recht „wild“ aussehen . . . .

## 6 Überprüfen Sie die Menge aus den folgenden Vektoren auf lin. Unabhängigkeit und geben Sie eine maximale (hinsichtlich Anzahl der Elemente) linear unabhängige Teilmenge an!
$\begin{pmatrix}
1 \\ 3 \\ 5 \\ 7
\end{pmatrix},\begin{pmatrix}
9 \\ 10 \\ 11 \\ 6
\end{pmatrix},\begin{pmatrix}
1 \\ 1 \\ 1 \\ 1
\end{pmatrix},\begin{pmatrix}
2 \\ 1 \\ 0 \\ -3
\end{pmatrix}$ 
